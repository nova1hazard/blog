<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //basic roles
        DB::table('roles')->insert([
            'role' => 'admin',
        ]);
        DB::table('roles')->insert([
            'role' => 'user',
        ]);
        //admin
        $role_id = DB::table('roles')->where('role', 'admin')->pluck('id');
        $user_id = DB::table('users')->insertGetId([
            'name' => 'Admin',
            'email' => 'nova1hazard@gmail.com',
            'password' => bcrypt('araaraara123'),
            'remember_token' => Str::random(60),
        ]);
        \App\UserRole::create(['user_id' => $user_id,
            'role_id' => $role_id[0]]);

        //basic admin sections
        $admin_sections = [
            ['section' => 'статьи', 'link' => 'article'],
            ['section' => 'web-проекты', 'link' => 'web'],
            ['section' => 'art-работы', 'link' => 'art'],
            ['section' => 'музыка', 'link' => 'music'],
            ['section' => 'видео', 'link' => 'video'],
            ['section' => 'dj-карьера', 'link' => 'dj'],
            ['section' => 'контакты', 'link' => 'contacts'],
        ];

        \App\AdminSection::insert($admin_sections);

        //basic header links
        $header = [
            ['link' => '/'],
            ['link' => 'articles'],
            ['link' => 'arts'],
            ['link' => 'music'],
            ['link' => 'web-projects'],
            ['link' => 'video-projects'],
            ['link' => 'dj-career'],
            ['link' => 'contacts'],
        ];

        \App\HeaderLinks::insert($header);
    }
}
