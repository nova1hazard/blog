<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFkToArticleHashtags extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('article_hashtags', function (Blueprint $table) {
            $table->foreign('article_id')->references('id')->on('articles')->onDelete('CASCADE');
            $table->foreign('hashtag_id')->references('id')->on('hashtags')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('article_hashtags', function (Blueprint $table) {
            $table->dropIfExists('article_id');
            $table->dropIfExists('hashtag_id');
        });
    }
}
