
//-----Scroll Menu Right Sidebar selection 
// Cache selectors


var lastId;
var  right_Menu = $(".right_sidebar");
        // All list items
var menuItems = right_Menu.find("a");
menuItems.splice(5, 1);
     // console.log(menuItems);
        // Anchors corresponding to menu items
var scrollItems = menuItems.map(function() {
            var item = $($(this).attr("href"));
            if (item.length) {
                return item;
            }
        });

// Bind click handler to menu items
// so we can get a fancy scroll animation
menuItems.click(function(e) {
  var href = $(this).attr("href"),
    offsetTop = href === "#" ? 0 : $(href).offset().top;
  $('html, body').stop().animate({
    scrollTop: offsetTop
  }, 300);

});

// Bind to scroll
$(window).scroll(function() {
  // Get container scroll position
  var fromTop = $(this).scrollTop();

  // Get id of current scroll item
  var cur = scrollItems.map(function() {
    if ($(this).offset().top <= fromTop+400)
      return this;
  });
  // Get the id of the current element
  cur = cur[cur.length - 1];
  var id = cur && cur.length ? cur[0].id : "";

  if (lastId !== id) {
    lastId = id;
    // Set/remove active class
    menuItems
      .children().removeClass("active_option")
      .end().filter("[href='#" + id + "']").children().addClass("active_option");
  }
});

