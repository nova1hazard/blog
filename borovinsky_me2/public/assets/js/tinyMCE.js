$(document).ready(function() {
    tinymce.init({
        selector:'textarea',
        a11y_advanced_options: true,
        image_advtab: true,
        image_class_list: [
            {title: 'Responsive', value: 'img-fluid'},
            {title: 'NoClass', value: 'no-class'}
        ],
        images_upload_url: '/admin/upload_image',
        image_uploadtab: true,
        plugins: [
            "advlist autolink image lists link image charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code fullscreen",
            "insertdatetime media nonbreaking save table directionality",
            "emoticons template paste textpattern imagetools"
        ],
        language:"ru",
        height: "500",
        toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
        toolbar2: "print preview media | forecolor backcolor emoticons",
        images_upload_handler: function (blobInfo, success, failure) {
            let xhr, formData;
            xhr = new XMLHttpRequest();
            xhr.withCredentials = false;
            // console.log($('meta[name="csrf-token"]').attr('content'));
            xhr.open('POST', '/admin/upload_image');
            xhr.setRequestHeader('X-CSRF-TOKEN', $('meta[name="csrf-token"]').attr('content'));
            xhr.onload = function() {
                let json;

                if (xhr.status != 200) {
                    failure('HTTP Error: ' + xhr.status);
                    return;
                }

                json = JSON.parse(xhr.responseText);

                if (!json || typeof json.location != 'string') {
                    failure('Invalid JSON: ' + xhr.responseText);
                    return;
                }

                success(json.location);
            };

            formData = new FormData();
            formData.append('file', blobInfo.blob(), blobInfo.filename());

            xhr.send(formData);
        },
    });
});