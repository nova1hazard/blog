var isMobile = false; //initiate as false
// device detection
if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent)
    || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0, 4))) isMobile = true;


//hidemenu first

$(function () {
    $(window).resize(function () {
        if (isMobile == false) {
            HideMenu();
        }

    })
});

if ($('.max_width_1230').is(':visible')) {
    $('.right_sidebar').hide();
}


$('.right_sidebar').hide();


sb_h = $('.right_sidebar').height();


var cssPaddingOn = {
    "padding-left": "10%",
    "padding-right": "10%"
}

var cssPaddingOff = {
    "padding-left": "0",
    "padding-right": "0"
}

function Hidebar() {
    $('.right_sidebar').hide();
}

function ShowMenu(button) {
    $('.right_sidebar').show();

    var w = $(window).width();


    rs_w = $('.right_sidebar').width();

    if ($('.max_width_768').is(':hidden')) {
        // $('.right_sidebar').height($(document).outerHeight(true));
        $('.right_sidebar').animate({left: 0}, 100, "linear");
        $('.big_offset').animate({marginLeft: rs_w}, 200, "linear");
        $('.code_menu_hide').hide();
        $('.code_menu_show').show();
        $('.line3').hide();
        $('.audio_player').animate({marginBottom: -100}, 500, "linear");
        $('.plr').css(cssPaddingOn);


        if ($('#main_page').is(':visible')) {
            itrh = $('.img_top_right').height();
            $('.img_top_right').css("height", "65%");
        }

        if ($('#audio_page').is(':visible')) {
            $('.audio_player_returner').animate({right: 0}, 500, "linear");
        }


    } else {

        // $('.right_sidebar').height('673');
        console.log(sb_h);
        $('.mt').css("padding-top", sb_h + 83);
        $('.mt_t2').css("margin-top", sb_h + 83);
        $('.right_sidebar').css({left: 0}, 0, "linear");
        $('.big_offset').css({marginLeft: 0}, 0, "linear");
    }


    $('.car').hide();

    $ //('#name-container').text('Lets Make Something Great');
    $('#name-container').css("border-top", "none");
    $('#name-container').css("border-bottom", "none");
    $('#name-container').css("padding-bottom", "0px");
    $('#name-container').css("width", "100%");

    $('.about_wrap').css("background", "#ffffff");
    $('.name').css("font-size", "16px");
    $('.email').css("font-size", "16px");
    $('.text').css("font-size", "14px");
    $('.date').css("font-size", "16px");
    //$('.hid_content').hide();

    $('.contact_info').css("font-size", "18px");
    $('.contacts_small_text').css("font-size", "14px");
    $('.hide_on_menu_show').hide();
    $('.show_on_menu_show').show();
    $(button).hide();

}


function HideMenu(button) {


    rs_w = $('.right_sidebar').width();

    var w = $(window).width();


    if ($('.max_width_768').is(':hidden')) {

        // $('.right_sidebar').height($(document).outerHeight(true));
        $('.right_sidebar').animate({left: -rs_w}, 200, "linear"),
            function () {
                $('.right_sidebar').hide(100);
            };
        $('.big_offset').animate({marginLeft: 0}, 100, "linear");
        $('.code_menu_show').hide();
        $('.code_menu_hide').show();
        $('.line3').show();
        $('.audio_player').animate({marginBottom: 0}, 500, "linear");
        $('.plr').css(cssPaddingOff);
        if ($('#main_page').is(':visible')) {
            $('.img_top_right').css("height", itrh);
        }
        if ($('#audio_page').is(':visible')) {
            $('.audio_player_returner').animate({right: -100}, 500, "linear");
        }
    } else {

        $('.mt').css("padding-top", 166);
        $('.mt_t2').css("margin-top", 79);


        // $('.code_menu_hide').show();
        $('.right_sidebar').css({left: -rs_w}, 0, "linear");
        $('.big_offset').css({marginLeft: 0}, 0, "linear");
    }

    $('.car').show();
    $('#name-container').css("border-top", "2px solid #fb6470");
    $('#name-container').css("border-bottom", "2px solid #fb6470");
    $('#name-container').css("padding-bottom", "20px");


    $('.about_wrap').css("background", "#f9f9f9");
    $('.name').css("font-size", "22px");
    $('.email').css("font-size", "20px");
    $('.text').css("font-size", "16px");
    $('.date').css("font-size", "18px");

    $('.contact_info').css("font-size", "22px");
    $('.contacts_small_text').css("font-size", "22px");

    $('#menu_button').show();

    $('.show_on_menu_show').hide();
    $('.hide_on_menu_show').show();
}


//------------------------------Button to top


var top_show = 150; // В каком положении полосы прокрутки начинать показ кнопки "Наверх"
var delay = 500; // Задержка прокрутки
$(document).ready(function () {
    $(window).scroll(function () { // При прокрутке попадаем в эту функцию
        /* В зависимости от положения полосы прокрукти и значения top_show, скрываем или открываем кнопку "Наверх" */
        if ($(this).scrollTop() > top_show) $('#to_top').fadeIn();
        else $('#to_top').fadeOut();
    });
    $('#to_top').click(function () { // При клике по кнопке "Наверх" попадаем в эту функцию
        /* Плавная прокрутка наверх */
        $('body, html').animate({
            scrollTop: 0
        }, delay);
    });
});

