<?php
 header('Content-type: image/png');
//header('Content-Type: text/html; charset=utf-8');
require_once('tokens.php');

class VKquery {

    public function getWall($wall_id,$count,$vvk,$servicetoken)
    {
        $result = file_get_contents("https://api.vk.com/method/wall.get?owner_id={$wall_id}&count=$count&v=$vvk&access_token=$servicetoken");
        $result_array = json_decode($result,true);
        return $result_array;

    }

    public function getComments($wall_id,$post_id,$count,$sort,$vvk,$servicetoken)
    {
        $result = file_get_contents("https://api.vk.com/method/wall.getComments?owner_id={$wall_id}&post_id={$post_id}&count=$count&sort=$sort&v=$vvk&access_token=$servicetoken");
        $result_array = json_decode($result,true);
        return $result_array;
    }

    public function getUser($from_id,$vvk,$respusertoken)
    {
        $result = file_get_contents("https://api.vk.com/method/users.get?&user_id=$from_id&fields=photo_400_orig&v=$vvk&access_token=".$respusertoken);
        $result_array = json_decode($result,true);
        return $result_array;
    }

    public function getGroupMembers($group_id,$vvk,$token)
    {
        $result = file_get_contents("https://api.vk.com/method/groups.getMembers?group_id=$group_id&sort=time_desc&count=1&v=$vvk&fields=photo_100&access_token=".$token);
        $result_array = json_decode($result, true);
        return $result_array;
    }

}

$VKquery = new VKquery();

$wallresult = $VKquery->getWall($wall_id,200,$vvk,$servicetoken);

$commresult = array();
$unix_comment_date = '0';

//Take 15 last posts and make array of comments

for ($i=0; $i < 15; $i++) {
    $post_id = $wallresult['response']['items'][$i]['id'];
    $commresult = $VKquery->getComments($wall_id,$post_id,'200','desc',$vvk,$servicetoken);
    $post_comm_count = $commresult['response']['count'];

//Seek for the last comment
    for ($j=0; $j < $post_comm_count; $j++) {

        if ($unix_comment_date > $commresult['response']['items'][$j + 1]['date']) {
            $date = $commresult['response']['items'][$j]['date'];
        } else {
            $commresult['response']['items'][$j + 1]['date'];
            $date = $commresult['response']['items'][$j + 1]['date'];
            $unix_comment_date = $commresult['response']['items'][$j + 1]['date'];
                if ($commresult['response']['items'][$j + 1]['text'] !== "") {
                    $lastcomment = $commresult['response']['items'][$j + 1]['text'];
                    $from_id = $commresult['response']['items'][$j + 1]['from_id'];
                }
        }
    }
}

//Cut and replace unnecessary charaters + add ...

$lastcomment = str_replace(array("\r", "\n"), array(),$lastcomment);
$lastcomment = preg_replace('/^[[]{1}id\d+|[0-9_.]\\|$/',"",$lastcomment);
$lastcomment = str_replace(array("]", "[", "|"), array(),$lastcomment);
$lastcomment = preg_replace('/id([^\/]\d+)/',"",$lastcomment);
$lastcomment = mb_substr($lastcomment, 0, 18);
$lastcomment = $lastcomment . '...';

//Catch exeption (if user is admin) then take user avatar/name/surname

if ($from_id < 0)
{
    $UserComName = 'ОЛДЫ';
    $UserLastComName = 'Республика';
}
else
{
    $User = $VKquery->getUser($from_id,$vvk,$respusertoken);
    $UserComName = $User['response'][0]['first_name'];
    $UserLastComName = $User['response'][0]['last_name'];
}

$UserComName  = $UserComName  .':';

//Take group members to get last subscriber

$GroupMembers = $VKquery->getGroupMembers($group_id,$vvk,$token);

//Get last subscriber
$LastSubscriberName = $GroupMembers['response']['items'][0]['first_name'];
$LastSubscriberLastName = $GroupMembers['response']['items'][0]['last_name'];
$LastSubscriberPhoto = $GroupMembers['response']['items'][0]['photo_100'];

//Create image from template + our data

$path = __DIR__;
$im = imagecreatefromjpeg($path.'/cover/1.jpg');
 
//Avatar
if ($LastSubscriberPhoto == "https://vk.com/images/camera_100.png?ava=1")
{
    $stamp = @imagecreatefromjpeg($path.'/cover/default.jpg');
}
else
{
    $stamp = @imagecreatefromjpeg($LastSubscriberPhoto);
}

//Set font + color
$white = @imagecolorallocate($im, 0, 0, 0);
$font = '/fonts/ArialRegular.ttf';

//Print last subscriber avatar
@imagecopy($im,$stamp,369,62,0,0,imagesx($stamp),imagesy($stamp));
//Print last subscriber name
@imagettftext($im,10,0,547,133,$white,$path.$font,$LastSubscriberName);
//Print last subscriber surname
@imagettftext($im,7,0,547,162,$white,$path.$font,$LastSubscriberLastName);

//Print last commentator name
@imagettftext($im,14,0,161,105,$white,$path.$font,$UserComName);
//Vivod last ommentator name comment
@imagettftext($im,10,0,132,135,$white,$path.$font,$lastcomment);

//create img and send it to use it in index php

//print_r($im);

imagejpeg($im, NULL, 100);
imagedestroy($im);

?>