<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 14.06.2020
 * Time: 20:53
 */

return [
    'hire' => 'Нанять меня',
    'login' => 'Войти',
    'register' => 'Регистрация',
    'logout' => 'Выйти',
    'send_message' => 'Ваше сообщение',
    'phone' => 'Телефон',
    'name' => 'Имя*',
    'email' => 'E-мейл*',
    'subject' => 'Тема*',
    'message' => 'Сообщение*',
    'location' => 'Адрес',
    'view_more' => 'Подробнее',
    'address' => 'Россия, Новосибирск',
];