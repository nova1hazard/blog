<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 29.04.2020
 * Time: 14:56
 */

return [
    'page_title' => 'BorovinskyMe',
    'about_me' => 'Обо мне',
    'work_exp' => 'Опыт работы',
    'education' => 'Образование',
    'my_skills' => 'Мои навыки',
    'top_subtitle' => '<p class="about_p">Я представляю себя как создателя различного контента: арт, музыка, видео, веб-сайты и скрипты.<br> Я обычно использую: <b>Laravel</b> | <b>Vue(Vuetify)</b> | <b>CSS</b> |
                            <b>SQL(PG)</b></p>
                        <div class="about"><span class="about_first">Имя: Артём Боровинский</span></div>
                        <div class="about"><span class="about_first">Возраст :</span> 27 лет</div>
                        <div class="about"><span class="about_first">Email :</span> nova1hazard@gmail.com</div>
                        <div class="about"><span class="about_first">Соцсети:</span> vk.com/graf_borovinsky</div>
                        <div class="about"><span class="about_first">Адрес: </span>Россия, Новосибирск
                        </div>',
    'work_1' => '<span class="name">Акцепт-Сиб</span>
                            <span class="email">https://acceptsib.ru/</span><br>
                            <span class="date">Янв 2019 - Фев 2019</span>
                            <p class="text">Моя первая работа в IT. Разработка на 1С Битрикс (php) и CSS.</p>',
    'work_2' => '<span class="name">Upstream</span>
                            <span class="email">https://upstream.team/</span><br>
                            <span class="date">Май 2019 - Июнь 2019</span>
                            <p class="text">Разработка на Laravel. Первая работа с фреймворком (было сложно).</p>',
    'work_3' => '<span class="name">ЦАН Новосибирск</span>
                            <span class="email">https://centralnoe.ru/</span><br>
                            <span class="date">Июль 2019 - Март 2020</span>
                            <p class="text">Laravel лендинги и Vue SPA приложение. Работа на аутсорсе.</p>',
    'work_4' => '<span class="name">Фриланс</span>
                            <span class="email">https://gbl.services/</span><br>
                            <span class="date">Июль 2019 - Сейчас</span>
                            <p class="text">Работа на фрилансе. Заказы сайтов (лендингов). Laravel/JS/CSS.</p>',
    'edu_1' => '<span class="name">Курсы повышения квалификации</span>
                            <span class="email">www.sipk.ru</span><br>
                            <span class="date">Сент 2007 - Май 2009</span>
                            <p class="text">Я изучал основы Delphi7, Pascal и другие языки программирования.</p>',
    'edu_2' => '<span class="name">Школа</span>
                            <span class="email">www.s_17.edu54.ru</span><br>
                            <span class="date">Сент 2000 - Май 2011</span>
                            <p class="text">Моя основная школа.</p>',
    'edu_3' => '<span class="name">НГАВТ Академия</span>
                            <span class="email">www.ssuwt.ru</span><br>
                            <span class="date">Сент 2011 - Июнь 2015</span>
                            <p class="text">Вуз, где я получил "высшее" образование. И много жизненного опыта).</p>',
    'edu_4' => '<span class="name">Фриланс</span>
                            <span class="email">http://borovinsky.me/</span><br>
                            <span class="date">Янв 2018 - Сейчас</span>
                            <p class="text">Самостоятельно изучал языки web-программирования, дизайн и linux.</p>',
];