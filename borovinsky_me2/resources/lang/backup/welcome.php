<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 29.04.2020
 * Time: 14:56
 */

return [
    'page_title' => 'BorovinskyMe',
    'about_me' => 'About Me',
    'work_exp' => 'Work Experience',
    'education' => 'Education',
    'my_skills' => 'My Skills',
    'top_subtitle' => '<p class="about_p">I represent myself as creator of different things: art, music, videos, web
                            pages, scripts.<br> I usually use: <b>PHP</b> | <b>JavaScript(JQ)</b> | <b>CSS</b> |
                            <b>SQL</b> | <b>HTML</b></p>
                        <div class="about"><span class="about_first">Name : Artem Borovinsky</span></div>
                        <div class="about"><span class="about_first">Age :</span> 26 Years</div>
                        <div class="about"><span class="about_first">Email :</span> nova1hazard@gmail.com</div>
                        <div class="about"><span class="about_first">Web :</span> vk.com/graf_borovinsky</div>
                        <div class="about"><span class="about_first">Address : </span>Russia, Novosibirsk, Kropotrin st.
                            128/3
                        </div>',
    'work_1' => '<span class="name">Courier</span>
                            <span class="email">www.novosibirsk.leverans.ru</span><br>
                            <span class="date">Jan 2017 - Marth 2017</span>
                            <p class="text">Delivered sushi and pizza in those times.</p>',
    'work_2' => '<span class="name">Php programmer</span>
                            <span class="email">www.acceptsib.ru</span><br>
                            <span class="date">Jan 2019 - Feb 2019</span>
                            <p class="text">I was engaged in the development of sites on the CMS "1C Bitrix".</p>',
    'work_3' => '<span class="name">Sound engineer</span>
                            <span class="email">www.ssuwt.ru</span><br>
                            <span class="date">Oct 2012 - Jan 2013</span>
                            <p class="text">I have experience of official work as a sound engineer at my university.</p>',
    'work_4' => '<span class="name">DJ</span>
                            <span class="email">www.vk.com/pcstudiodj</span><br>
                            <span class="date">Oct 2012 - Now</span>
                            <p class="text">The best years of my life! DJ job from PC DJ Studio group.</p>',
    'edu_1' => '<span class="name">Programming school</span>
                            <span class="email">www.sipk.ru</span><br>
                            <span class="date">SEP 2007 - MAY 2009</span>
                            <p class="text">I studied Delphi7, Pascal and other basic programming stuff for those
                                time.</p>',
    'edu_2' => '<span class="name">Driver courses</span>
                            <span class="email">www.avtolux2002.ru</span><br>
                            <span class="date">SEP 2016 - DEC 2016</span>
                            <p class="text">I do not know why I went to study driving a car, because I do not like this
                                thing!</p>',
    'edu_3' => '<span class="name">School</span>
                            <span class="email">www.s_17.edu54.ru</span><br>
                            <span class="date">Sep 2000 - May 2011</span>
                            <p class="text">My dear favorite school where I spent my childhood with the best
                                teachers.</p>',
    'edu_4' => '<span class="name">NSAWT University</span>
                            <span class="email">www.ssuwt.ru</span><br>
                            <span class="date">SEP 2011 - JUN 2015</span>
                            <p class="text">I learned how to be a slave in modern society and met many friends.</p>',




];