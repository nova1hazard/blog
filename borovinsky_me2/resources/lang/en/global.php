<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 14.06.2020
 * Time: 20:53
 */

return [
    'hire' => 'Hire Me!',
    'login' => 'Login',
    'register' => 'Register',
    'logout' => 'Logout',
    'send_message' => 'Send Message',
    'phone' => 'Phone',
    'name' => 'Name*',
    'email' => 'E-mail*',
    'subject' => 'Subject*',
    'message' => 'Message*',
    'location' => 'Location',
    'view_more' => 'View more',
    'address' => 'Russia, Novosibirsk',
];

