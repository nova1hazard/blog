<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 29.04.2020
 * Time: 14:56
 */

return [
    'page_title' => 'BorovinskyMe',
    'about_me' => 'About Me',
    'work_exp' => 'Work Experience',
    'education' => 'Education',
    'my_skills' => 'My Skills',
    'top_subtitle' => '<p class="about_p">I represent myself as creator of different things: art, music, videos, web
                            pages, scripts.<br> I usually use: <b>PHP</b> | <b>JavaScript(JQ)</b> | <b>CSS</b> |
                            <b>SQL</b> | <b>HTML</b></p>
                        <div class="about"><span class="about_first">Name : Artem Borovinsky</span></div>
                        <div class="about"><span class="about_first">Age :</span> 27 Years</div>
                        <div class="about"><span class="about_first">Email :</span> nova1hazard@gmail.com</div>
                        <div class="about"><span class="about_first">Web :</span> vk.com/graf_borovinsky</div>
                        <div class="about"><span class="about_first">Address : </span>Russia, Novosibirsk</div>',
    'work_1' => '<span class="name">Php programmer</span>
                            <span class="email">www.acceptsib.ru</span><br>
                            <span class="date">Jan 2019 - Feb 2019</span>
                            <p class="text">I was engaged in the development of sites on the CMS "1C Bitrix".</p>',
    'work_2' => '<span class="name">Upstream</span>
                            <span class="email">https://upstream.team/</span><br>
                            <span class="date">May 2019 - Jun 2019</span>
                            <p class="text">I was develop on Laravel. My first work with framework (it was hard).</p>',
    'work_3' => '<span class="name">CAN Novosibirsk</span>
                            <span class="email">https://centralnoe.ru/</span><br>
                            <span class="date">JUL 2019 - MAR 2020</span>
                            <p class="text">Laravel landings and Vue SPA. Outsource free work for realty agency.</p>',
    'work_4' => '<span class="name">Freelance</span>
                            <span class="email">https://gbl.services/</span><br>
                            <span class="date">JUL 2019 - Now</span>
                            <p class="text">Freelance work. Some offers to do. Laravel/JS/CSS.</p>',
    'edu_1' => '<span class="name">Programming school</span>
                            <span class="email">www.sipk.ru</span><br>
                            <span class="date">SEP 2007 - MAY 2009</span>
                            <p class="text">Freelance work. Some offers to do. Laravel/JS/CSS.</p>',
    'edu_2' => '<span class="name">School</span>
                            <span class="email">www.s_17.edu54.ru</span><br>
                            <span class="date">Sep 2000 - May 2011</span>
                            <p class="text">My dear favorite school where I spent my childhood with the best
                                teachers.</p>',
    'edu_3' => '<span class="name">NSAWT University</span>
                            <span class="email">www.ssuwt.ru</span><br>
                            <span class="date">SEP 2011 - JUN 2015</span>
                            <p class="text">I learned how to be a slave in modern society and met many friends.</p>',
    'edu_4' => '<span class="name">Freelance study</span>
                            <span class="email">http://borovinsky.me/</span><br>
                            <span class="date">Jan 2018 - Now</span>
                            <p class="text">Years of learning programming languages.</p>',




];