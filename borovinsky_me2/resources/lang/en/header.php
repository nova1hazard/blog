<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 29.04.2020
 * Time: 14:56
 */

return [
    'me' => 'Artem Borovinsky',
    'prof' => 'Creative Creator',
    'developer' => 'Web Developer',
    'lets' => 'LETS MAKE SOMETHING GREAT',
    'contact_me' => 'Contact me if you are interested in my personality or my work. let\'s make projects together!',
    //menu
    'home' => 'Home',
    'about_me' => 'About me',
    'work_exp' => 'Work Experience',
    'edu' => 'Education',
    'skills' => 'Skills',
    'port' => 'Portfolio',
    'contacts' => 'Contacts',
];