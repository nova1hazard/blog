<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 02.07.2020
 * Time: 20:36
 */
return [

    'music-title' => 'Music',
    'music' => '<img src="/assets/images/sdsd.jpg" style="float:left; width: 135px;" class="pr-2">I have never been a musician, but I loved listening to music and wanted to write my own
                            tunes. I began studying the <b>FL Studio</b> sequencer in 2012. The first successes began
                            very late. In 2013, I learned to write about melodies, and in 2014 I began to do something
                            similar to full-fledged tracks. I started learning <b>Ableton Live</b> that year. 2015 was
                            the best year for writing music, I did a lot of good work, I was often visited by
                            inspiration then. After the creative crisis began in 2016, I was forced to stop composing
                            music.<br><br> In 2017-18, I began to write again, but on a weak computer, the quality of
                            work deteriorated. I plan to go back to writing music in future. P.S. - I did all the basses
                            and some of other sounds by myself.',
    'music-subtitle' => 'Here are collected music, which I made with <b>FL Studio</b> & <b>Ableton Live</b> sequencers.
Click on the record in playlist to listen to the track. And shure you can rate this division
                        below.',
    'videos-title' => 'Videos',
    'videos' => '<img src="/assets/images/boomer.gif" style="float:left; " class="pr-2">I first became interested in video editing in 2009.
                    At that time I loved to watching anime and various films.
                    Since then, I began to study <b>Sony Vegas</b>.
                    The first works weren\'t very successful. But I began to devote a lot of time to working on the video.
                    The first serious work took about 3 months and came out under the name "Dracula Rampage".
                    It was  <b>AMV*</b>, which I posted on the site
                    amvnews. Having received good grades from my work, I started working on the next project, but unfortunately my computer could not stand rendering and I stopped creating a couple of small videos.<br>
<blockquote class="text-info smalltx"><br>AMV* - An anime music video (AMV), typically is a fan-made music video consisting of clips from one or more Japanese animated shows or movies set to an audio track, often songs or promotional trailer audio.</blockquote><br><br>
                    <span class="dots">...</span><br>
                    I returned to the video in 2014, when I had the idea to write music and make something like a video clip about the collapse of the USSR. There was a lot of work, I went through piles of different sources and finally finished this clip. Critics on the creaspace site did not clearly meet him,
                    There were a lot of negative ratings due to the theme of the clip.',
    'web-title' => 'Web projects',
    'web' => '<blockquote class="bot_header blockquote-footer"> html -> php -> sql -> css -> js -> jq ->
                                laravel -> vue -> vuetify
                            </blockquote>
                            <br>


                            I have started to learn web programming in February 2018. <br> <br>
                            Before that, I didn’t have any special skills and knew almost nothing about it. <br> <br>
                            At first it was interesting to create scripts for social network <b>"VKontakte"</b> (Russian
                            Facebook).
                            Then i started to learn other programming languages ​​(css, js, sql, html). <br><br>And now
                            i\'m on the point of <b>"Laravel"</b> framework learning.<br><br>
                            <pre>&lt? php echo "Hello World" ?&gt</pre>',
    'dj-title' => 'My Dj way',


];