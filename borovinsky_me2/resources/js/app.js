/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Admin and other libraries. It is a great starting point when
 * building robust, powerful web applications using Admin and Laravel.
 */
require('intersection-observer');
require('./bootstrap');

import Vuetify from 'vuetify';
import 'vuetify/dist/vuetify.min.css';

window.Vue = require('vue');

Vue.use(Vuetify);
/**
 * The following block of code may be used to automatically register your
 * Admin components. It will recursively scan this directory for the Admin
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Admin.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('article-category', require('./components/admin/article/ArticleCategory.vue').default);
Vue.component('article-hashtags', require('./components/admin/article/ArticleHashtags.vue').default);
Vue.component('art-category', require('./components/admin/art/ArtCategory.vue').default);
Vue.component('art-uploader', require('./components/admin/art/ArtUploader.vue').default);
Vue.component('v-art-crud', require('./components/admin/art/V-art-crud.vue').default);
Vue.component('web-category', require('./components/admin/web/WebCategory.vue').default);
Vue.component('v-music-crud', require('./components/admin/music/V-music-crud.vue').default);
Vue.component('music-genres', require('./components/admin/music/MusicGeneres.vue').default);
Vue.component('v-dj-crud', require('./components/admin/dj/V-dj-crud.vue').default);
Vue.component('dj-category', require('./components/admin/dj/DjCategory.vue').default);
Vue.component('dj-uploader', require('./components/admin/dj/DjUploader.vue').default);
Vue.component('text-content', require('./components/admin/content/TextContent.vue').default);
// Vue.component('vue-ck-editor', require('./components/VueCkEditor.vue').default);

Vue.component('comment-form', require('./components/CommentForm.vue').default);
Vue.component('music', require('./components/Music.vue').default);
Vue.component('gallery', require('./components/Gallery.vue').default);
Vue.component('excel-parser', require('./components/ExcelParser.vue').default);

Vue.config.productionTip = false;
Vue.config.devtools = false;

window.onload = function () {
    const app = new Vue ({
        el: '#app',
        vuetify: new Vuetify(),
        components: {
        },
    });
};



