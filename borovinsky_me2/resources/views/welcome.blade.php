@extends('layouts.app')

@section('content')
    <meta name="description" content="Я - веб разработчик, представляю себя как создателя различного контента: арт, музыка, видео, веб-сайты и скрипты.">
    <a id="About_me"></a>
    <div class="about_wrap mt ">
        <div class="container">
            <div class="plr">
                <div class="row">
                    <div class="col-md-5">
                        <span class="block_header ">@lang("welcome.about_me")</span>
                        <div class="line"></div>
                        @lang("welcome.top_subtitle")
                    </div>
                    <div class="col-md-5 offset-md-2">
                        <img src="assets/images/me2.png" alt="me" class="img-fluid img_top_right">
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if(isset($portfolios))
    <a id="Portfolio" >
        <meta name="description" content="Разделы направлений моей работы.">
        <div class="container">
            <a id="Portfolio"></a>
            <div class="plr">
                <div class="row">
                    <div class="col-md-5">
                        <span class="block_header mt_t2">@lang('header.port')</span>
                        <div class="line"></div>
                    </div>
                </div>
            </div>
            <div class="plr">
                <div class="row">

                    @foreach($portfolios as $k=>$portfolio)

                        <div class="col-md-6">
                            <div class="project_block">
                                <img src="/storage/{{$portfolio->image}}" class="img-fluid" alt="portfolio">
                                <div class="project_block_inner">{{$portfolio->title}}<br>
                                    <span>{!! $portfolio->text !!}</span>
                                </div>
                                <a href="/portfolio/{{ $portfolio->link}}#project">@lang('global.view_more') ></a>
                            </div>
                        </div>

                    @endforeach
                </div>
            </div>
        </div>
    </a>
    @endif
    <a id="Work_Experience"></a>
    <div class="container">
        <div class="plr">
            <div class="row">
                <div class="col-md-12">
                    <span class="block_header mt_t">@lang("welcome.work_exp")</span>
                    <div class="line"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="plr">
            <div class="row">
                <div class="col-md-5">
                    <div class="info_block_r">
                        <div class="info_block_inner">
                            @lang("welcome.work_2")
                        </div>
                    </div>
                    <div class="info_block_r mt_194">
                        <div class="info_block_inner">
                            @lang("welcome.work_4")
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="scheme_mid">
                        <div class="circle">
                            <div class="circle_inner">
                            </div>
                        </div>
                        <div class="circle mt_160">
                            <div class="circle_inner">
                            </div>
                        </div>
                        <div class="circle mt_96">
                            <div class="circle_inner">
                            </div>
                        </div>
                        <div class="circle mt_160">
                            <div class="circle_inner">
                            </div>
                        </div>
                        <div class="line_wrap">
                            <div class="vertical_line">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="info_block_l">
                        <div class="info_block_inner">
                            @lang("welcome.work_1")
                        </div>
                    </div>
                    <div class="info_block_l mt_194">
                        <div class="info_block_inner">
                            @lang("welcome.work_3")
                        </div>
                    </div>
                </div>
            </div>
            <div class="line2"></div>
        </div>
    </div>
    <a id="Education"></a>
    <div class="container">
        <div class="plr">
            <div class="row">
                <div class="col-md-12">
                    <span class="block_header mt_t">@lang("welcome.education")</span>
                    <div class="line"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="plr">
            <div class="row">
                <div class="col-md-5">
                    <div class="info_block_r">
                        <div class="info_block_inner">
                            @lang("welcome.edu_2")
                        </div>
                    </div>
                    <div class="info_block_r mt_194">
                        <div class="info_block_inner">
                            @lang("welcome.edu_4")
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="scheme_mid">
                        <div class="circle">
                            <div class="circle_inner">
                            </div>
                        </div>
                        <div class="circle mt_160">
                            <div class="circle_inner">
                            </div>
                        </div>
                        <div class="circle mt_96">
                            <div class="circle_inner">
                            </div>
                        </div>
                        <div class="circle mt_160">
                            <div class="circle_inner">
                            </div>
                        </div>
                        <div class="line_wrap">
                            <div class="vertical_line">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="info_block_l">
                        <div class="info_block_inner">
                            @lang("welcome.edu_1")
                        </div>
                    </div>
                    <div class="info_block_l mt_194">
                        <div class="info_block_inner">
                            @lang("welcome.edu_3")
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="myskills">
        <div class="container">
            <a id="Skills"></a>

            <div class="plr">

                <div class="row">
                    <div class="col-md-12">
                        <span class="block_header mt_64_w">@lang("welcome.my_skills")</span>
                        <div class="line"></div>
                    </div>
                </div>

            </div>
            <div class="plr">
                <div class="container" style="padding: 0px">
                    <div class="row">
                        <div class="col-11 col-md-3">
                            <div class="load_span">
                                Laravel
                            </div>
                            <br>
                            <div class="load_line">
                                <div class="load_fill" style="width: 46%">
                                    <div class="load_circle" style="left: calc(46% - 9px" )
                                    ">
                                </div>
                            </div>
                        </div>
                        <div class="load_span">
                            Vue
                        </div>
                        <br>
                        <div class="load_line">
                            <div class="load_fill" style="width: 32%">
                                <div class="load_circle" style="left: calc(32% - 9px" )
                                ">
                            </div>
                        </div>
                    </div>
                    <div class="load_span">
                        SCSS
                    </div>
                    <br>
                    <div class="load_line">
                        <div class="load_fill" style="width: 48%">
                            <div class="load_circle" style="left: calc(48% - 9px" )
                            ">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-1 col-md-1">
                <div class="percent_load">
                    46%
                </div>
                <div class="percent_load">
                    32%
                </div>
                <div class="percent_load">
                    48%
                </div>
            </div>
            <div class="col-11 col-md-3">
                <div class="load_span">
                    PSQL
                </div>
                <br>
                <div class="load_line">
                    <div class="load_fill" style="width: 35%">
                        <div class="load_circle" style="left: calc(35% - 9px" )
                        ">
                    </div>
                </div>
            </div>
            <div class="load_span">
                Docker
            </div>
            <br>
            <div class="load_line">
                <div class="load_fill" style="width: 20%">
                    <div class="load_circle" style="left: calc(20% - 9px" )
                    ">
                </div>
            </div>
        </div>
        <div class="load_span">
            Ubuntu
        </div>
        <br>
        <div class="load_line">
            <div class="load_fill" style="width: 30%">
                <div class="load_circle" style="left: calc(30% - 9px" )
                ">
            </div>
        </div>
    </div>
    </div>
    <div class="col-1 col-md-1">
        <div class="percent_load">
            35%
        </div>
        <div class="percent_load">
            20%
        </div>
        <div class="percent_load">
            30%
        </div>
    </div>
    <div class="col-11 col-md-3">
        <div class="load_span">
            PHP
        </div>
        <br>
        <div class="load_line">
            <div class="load_fill" style="width: 43%">
                <div class="load_circle" style="left: calc(43% - 9px" )
                ">
            </div>
        </div>
    </div>
    <div class="load_span">
        JS
    </div><br>
    <div class="load_line">
        <div class="load_fill" style="width: 40%">
            <div class="load_circle" style="left: calc(40% - 9px" )
            ">
        </div>
    </div>
    </div>
    <div class="load_span">
        Web design
    </div><br>
    <div class="load_line">
        <div class="load_fill" style="width: 10%">
            <div class="load_circle" style="left: calc(10% - 9px" )
            ">
        </div>
    </div>
    </div>
    </div>
    <div class="col-1 col-md-1">
        <div class="percent_load">
            43%
        </div>
        <div class="percent_load">
            40%
        </div>
        <div class="percent_load">
            10%
        </div>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
@endsection

<script src="https://cdn.jsdelivr.net/gh/cferdinandi/smooth-scroll@15.0/dist/smooth-scroll.polyfills.min.js"></script>
<script>
    var scroll = new SmoothScroll('a[href*="#"]');
</script>



