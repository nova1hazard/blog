
<div class="row mg_0">
    <div class="col-md-12 col-sm-12 mg_0 p-0">
        <div class="right_sidebar">
            <i class="fa fa-times fa-2x" onclick="HideMenu(this)"></i>
            <div class="side_img">
                <div class="side_img_text">@lang("header.me")<br><span>@lang("header.prof")</span></div>
                <div class="max_width_768"></div>
                <div class="max_width_1230"></div>
            </div>
            <a href="{{ route(('home')).'#Home'}}">
                <div class="menu_option active_option">@lang("header.home")</div>
            </a>
            <a href="#About_me">
                <div class="menu_option">@lang("header.about_me")</div>
            </a>
            <a href="#Work_Experience">
                <div class="menu_option">@lang("header.work_exp")</div>
            </a>
            <a href="#Education">
                <div class="menu_option">@lang("header.edu")</div>
            </a>
            <a href="#Skills">
                <div class="menu_option">@lang("header.skills")</div>
            </a>
            <a href="#Portfolio">
                <div class="menu_option">@lang("header.port")</div>
            </a>
            <a href="#Contacts">
                <div class="menu_option">@lang("header.contacts")</div>
            </a>

            {{--авторизация если нужно--}}
            {{--<section class="max_width_768">--}}
            {{--@guest--}}
                {{--<a href="{{ route('login') }}">--}}
                    {{--<div class="menu_option">@lang('global.login')</div>--}}
                {{--</a>--}}
                {{--@if (Route::has('register'))--}}
                    {{--<a href="{{ route('register') }}">--}}
                        {{--<div class="menu_option">@lang('global.register')</div>--}}
                    {{--</a>--}}
                {{--@endif--}}
            {{--@else--}}
                {{--<a href="{{ route('logout') }}"--}}
                   {{--onclick="event.preventDefault();--}}
                            {{--document.getElementById('logout-form').submit();">--}}
                    {{--<div class="menu_option">--}}
                        {{--@lang('global.logout')--}}
                    {{--</div>--}}
                {{--</a>--}}

                {{--<form id="logout-form" action="{{ route('logout') }}" method="POST"--}}
                      {{--style="display: none;">--}}
                    {{--@csrf--}}
                {{--</form>--}}

            {{--@endguest--}}
            {{--</section>--}}
        </div>
    </div>
</div>
<a id="Home"></a>

<div  class="top_block">
    {{--<video src="{{ asset('assets/video/test33.mp4')}}" class="my-background-video  jquery-background-video" autoplay loop muted></video>--}}
    <img class="top_overlay show_on_menu_show" src="{{asset('assets/images/back21.jpg')}}">
    <div class="container">
        <div class="row">
            <!--Navbar-->
            <nav class="navbar top_menu">
                <!-- Collapse button -->
                <button style="margin-left: 1px;" class="navbar-toggler toggler-example" id="menu_button"
                        onclick="ShowMenu(this)"
                        type="button">
                    <span class="dark-blue-text">
                        <i class="fa fa-bars fa-2x"></i>
                    </span>
                </button>

                <div class="localization_container hide_on_menu_show">
                    <a href="/"><img src="{{asset('assets/images/ru.gif')}}" class="flag_button loc_item" alt="RU"></a>
                    <a href="/en"><img src="{{asset('assets/images/en.gif')}}" class="flag_button loc_item" alt="EN"></a>
                </div>
            </nav>
            <!--/.Navbar-->
        </div>
        <section class="top_text_wrap">
            <div class="code_menu_show">
                <div class='col-md-8 offset-md-2'>
                    <div id='name-container'>@lang("header.lets")</div>
                    <span class='desc desc_bot'>@lang("header.contact_me")</span><a
                            href='https://vk.com/graf_borovinsky'>
                        <button type='button'
                                class='btn btn-danger hire_btn hire_btn_bot'>@lang("global.hire")</button>
                    </a></div>
            </div>
            <div class="code_menu_hide">
                <div class="col-md-4 offset-md-5">
                    <div id="name-container">@lang("header.me")</div>

                    <span class="desc">#@lang("header.prof")
                        &#160&#160&#160|&#160&#160&#160#@lang("header.developer")</span>
                    <a href="https://vk.com/graf_borovinsky">
                        <button type="button" class="btn btn-danger hire_btn">@lang("global.hire")</button>
                    </a>
                </div>
            </div>
        </section>
    </div>
</div>
