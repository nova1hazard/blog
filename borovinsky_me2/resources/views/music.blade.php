@extends('layouts.app')

@section('content')
    <meta name="description" content="Музыка, которую я сочинил сам.">
    <div>
        <!--  audio player returner-->
        <div id="audio_page">
            <!--[if !IE]-->
            <div class="audio_player_returner" onclick="HideMenu()">
                <img src="{{asset('assets/images/icons/wave.png')}}" title='Show audio player' alt='player'>
            </div>
        </div>

        <a id="project"></a>
        <div class="container">
            <div class="plr">
                  <span class="block_header mt_t2">@lang("pages.music-title")
                  </span>
                <div class="line"></div>
                <div class="text_block">
                    <!--<img class="img-fluid in_text_img" src="projects/music/sn.png" alt="article-image"> -->
                    @lang("pages.music")
                </div>
                <div class="project_text">
                    @lang("pages.music-subtitle")
                </div>
                <!--
                    playlist
                     -->
                <div>
                    @if($musics)
                        <music :data='@json($musics)'></music>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection