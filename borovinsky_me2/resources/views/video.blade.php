@extends('layouts.app')

@section('content')

    <a id="project"></a>
    <meta name="description" content="Мои видео работы.">
    <div class="container">
        <div class="plr">
            <div class="row">
                <div class="col-md-12">
                    <span class="block_header mt_t2">@lang("pages.videos-title")</span>
                    <div class="line"></div>
                </div>
                <div class="project_text">
                    @lang("pages.videos")
                </div>
                <div class="project_text">

                    @forelse($videos as $video)
                        <span class="script_header"><img src="{{asset('assets/images/icons/biohazard.svg')}}"
                                                         class="icon_prod" alt="video">{{$video->title}}</span><br>
                        <a class="video_helper" href="{{$video->original_link}}">
                            Если у вас возникли проблемы с видео, нажмите на эту ссылку. <i
                                    class="fas fa-long-arrow-alt-left"></i></a><br><br>
                        <p align="center">
                            <video class="video-proj" controls poster="/storage/{{$video->image}}">
                                <source class="video_frame" src="/storage/{{$video->video}}" type="video/ogg">
                            </video>
                        </p>

                        <dl>
                            {!! $video->text !!}
                        </dl>
                        <br>
                        <div class="line4"></div>
                    @empty
                    @endforelse
                </div>
            </div>
        </div>
    </div>

    <style>
        .video-proj {
            width: 100%;
            height: auto;
            margin-bottom: 20px;
            max-width: 640px;
            max-height: 480px;
        }
    </style>

@endsection