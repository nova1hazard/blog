@extends('layouts.app')

@section('content')
    <meta name="description" content="Моя карьера диджея.">
    <a id="project"></a>
    <div class="container">
        <div class="plr">
                <span class="block_header mt_t2">@lang("pages.dj-title")</span>
                <div class="line"></div>
                @forelse($all_djs as $dj_cat)
                    <div class="project_text" style="margin-bottom: 0px;">
                        <span class="script_header">{{$dj_cat['category']}} ({{count($dj_cat['dj'])}})</span><br><br>
                        {{$dj_cat['text']}}
                    </div>
                    <gallery :data='@json($dj_cat['dj'])'></gallery>
                @empty
                @endforelse
        </div>
    </div>

@endsection