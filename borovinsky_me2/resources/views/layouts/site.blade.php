<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">
<title>@lang("welcome.page_title")</title>

<!-- Fonts -->
<!--Montserrat font-->
<link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">

<link rel="shortcut icon" href="{{ asset('assets/images/BR.png') }}" type="image/png">
<!-- Styles -->
<link href="https://fonts.googleapis.com/css2?family=Rubik+Mono+One&display=swap" rel="stylesheet">
<link href="{{ asset('assets/css/rangesliders.css')}}" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="{{ asset('assets/js/baguetteBox.js-dev/dist/baguetteBox.min.css')}}">
<link href="https://cdn.jsdelivr.net/npm/@mdi/font@5.x/css/materialdesignicons.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css"
      integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
      integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/js/jquery-background-video/jquery.background-video.css')}}">
<link href="{{ asset('/css/app.css')}}" rel="stylesheet" type="text/css">
<!-- Scripts -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.js"></script>

