<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @include('layouts.site')
</head>

<body>
<div>
    <main>
        <div class="big_offset">
            @include('header')
            <div id="app">
                @yield('content')
                 {{--комментарии добавляем если есть link_id--}}
                @if(isset($link_id))
                    <comment-form :link_id='@json($link_id)'></comment-form>
                 @endif
            </div>
            @include('contacts')
            @include('footer')
            <div id="to_top"><i class="fas fa-2x fa-sort-up"></i></div>
        </div>
    </main>
</div>
</body>

@include('layouts.scripts')
</html>
