
<div class="container">
    <div class="plr">
        <footer>

            <div class="line2"></div>

            <div class="row">

                <div class="col-md-4 offset-md-4">
                    <div class="contacts_bottom">
                        <div class="icon_div">
                            <a href="https://vk.com/graf_borovinsky" target="_blank">
                                <img src="{{asset('assets/images/icons/vk.svg')}}" alt="vk">
                            </a>
                            <div class="gray_line"></div>
                            <a href="https://wa.me/79137833910" target="_blank">
                                <img src="{{asset('assets/images/icons/whatsapp.svg')}}" alt="whatsapp">
                            </a>
                            <div class="gray_line"></div>
                            <a href="https://discordapp.com/invite/kUF7vAn" target="_blank">
                                <img src="{{asset('assets/images/icons/discord.svg')}}" alt="discord">
                            </a>
                        </div>
                        Copyright: Junaed Ahmed, All rights reserved
                    </div>
                </div>

            </div>

        </footer>

    </div>
</div>