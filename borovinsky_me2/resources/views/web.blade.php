@extends('layouts.app')

@section('content')

    <meta name="description" content="Мои различные веб-проекты.">
    <a id="project"></a>
    <div class="container">
        <div class="plr">
                    <span class="block_header mt_t2">@lang("pages.web-title")
                    </span>
                    <div class="line"></div>
                <div class="project_text">
                    <div class="row">

                        <div class="col-md-6">
                            @lang("pages.web")
                        </div>
                        <div class="col-md-6">
                            <img src="{{asset('assets/images/php_fesh_logo.png')}}" alt="php"
                                 class="img-fluid col_side_img">
                        </div>
                    </div>
                </div>
                <div class="project_text">
                    @forelse($webs as $web)
                        <hr>
                        <span class="script_header">
                            <span class="rubik">
                            <i class="fas fa-code"></i> {{$web->title}}
                            </span>
                            <i class="far fa-calendar-check"></i>
                            {{date("d.m.Y",strtotime($web->project_date))}}
                        </span>
                        <br>
                         <a class="" href="{{$web->original_link}}">{{$web->original_link}}</a>
                        <br><br>
                        {!! $web->text !!}<br><br>
                    @empty
                    @endforelse
                </div>
        </div>
    </div>


@endsection