@extends('layouts.app')

@section('content')
    <meta name="description" content="Разделы направлений моей работы.">
    <div class="container">
        <a id="Portfolio"></a>
        <div class="plr">
            <div class="row">
                <div class="col-md-5">
                    <span class="block_header mt_t2">@lang('header.port')</span>
                    <div class="line"></div>
                </div>
            </div>
        </div>
        <div class="plr">
            <div class="row">
                {{--<div class="col-md-12">--}}
                    {{--<div class="line3">--}}
                        {{--<ul>--}}
                            {{--<li id="first_elem">Art</li>--}}
                            {{--<li>Music producing/DJ</li>--}}
                            {{--<li>Social groups</li>--}}
                            {{--<li>Web projects</li>--}}
                        {{--</ul>--}}
                    {{--</div>--}}
                {{--</div>--}}
            </div>
        </div>
        <div class="plr">
            <div class="row">

                @foreach($portfolios as $k=>$portfolio)

                    <div class="col-md-6">
                        <div class="project_block">
                            <img src="/storage/{{$portfolio->image}}" class="img-fluid" alt="portfolio">
                            <div class="project_block_inner">{{$portfolio->title}}<br>
                                <span>{!! $portfolio->text !!}</span>
                            </div>
                            <a href="/portfolio/{{ $portfolio->link}}#project">@lang('global.view_more') ></a>
                        </div>
                    </div>

                @endforeach
            </div>
        </div>
    </div>
@endsection

