@extends('layouts.admin')

@section('admin-content')
    @if (session('status'))
        <div class="alert alert-success mb-4">
            {{ session('status') }}
        </div>
    @endif

    <div class="list-group mb-4 card">
                    <span class="list-group-item list-group-item-action active">
                        Видео ({{$videos->count()}})
                    </span>
        <div class="card-body">
            <table class="table table-sm">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Заголовок</th>
                    <th scope="col">Обложка</th>
                    <th scope="col">Текст</th>
                    <th scope="col">Дата</th>
                    {{--<th scope="col">Хештеги</th>--}}
                    <th scope="col">Оригинальная ссылка</th>
                    <th scope="col" class="text-right">Действия</th>
                </tr>
                </thead>
                <tbody>
                @foreach($videos as $k=>$video)

                    <tr>
                        <th scope="row">{{$k+1}}</th>
                        <td><b>{{$video->title}}</b></td>
                        <td><img src="/storage/{{$video->image}}" style="max-width: 140px;" class="img-fluid" alt=""></td>
                        <td> {!! \Illuminate\Support\Str::limit($video->text, 30) !!}...</td>
                        {{--<td>--}}
                        {{--<ul>--}}
                        {{--@forelse($video->hashtag as $hashtag)--}}
                        {{--<li>{{$hashtag['hashtag']}}</li>--}}
                        {{--@empty--}}
                        {{--@endforelse--}}
                        {{--</ul>--}}
                        {{--</td>--}}
                        <td>{{$video->date}}</td>
                        <td>{{$video->original_link}}</td>
                        <td class="text-right">
                            <a href="/admin/video/{{$video->id}}/edit" class="btn btn-primary" role="button"
                               aria-pressed="true"><i class="fas fa-edit"></i></a>
                            <a href="/admin/video/{{$video->id}}" class="btn btn-danger" role="button"
                               onclick="event.preventDefault();
                        document.getElementById('delete-form').submit();">
                                <i class="fas fa-minus-square"></i>
                            </a>
                            <form id="delete-form" action="/admin/video/{{$video->id}}" method="POST"
                                  style="display: none;">
                                <input name="_method" type="hidden" value="DELETE">
                                @csrf
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <a href="/admin/video/create" class="btn btn-primary mb-4" role="button"
       aria-pressed="true"><i class="fas fa-plus-square mr-2"></i>Добавить видео
    </a>
@endsection