@extends('layouts.admin')

@section('admin-content')

    <h2 class="bd-title mb-4">Новое видео</h2>
    <form action="/admin/video" method="POST" enctype='multipart/form-data'>
        @csrf

        <div class="row mb-3">
            <div class="col-md-4">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <label class="input-group-text">Заголовок</label>
                    </div>
                    <input name="title" class="form-control" id="title">
                </div>
            </div>
            <div class="col-md-4">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <label class="input-group-text">Дата</label>
                    </div>
                    <input type="date" class="form-control mydate" name="date" placeholder="Дата" id="date">
                </div>
            </div>
            <div class="col-md-4">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <label class="input-group-text">Оригинальная ссылка</label>
                    </div>
                    <input class="form-control mydate" name="original_link" id="original_link">
                </div>
            </div>
        </div>


        <div class="form-group mb-4">
            <textarea class="form-control" name="text" rows="6"></textarea>
        </div>

        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <label class="input-group-text">Видео</label>
            </div>
            <div class="custom-file">
                <input type="file" name="video" class="custom-file-input" id="video">
                <label class="custom-file-label custom-file-label-video" for="video">Choose file</label>
            </div>
        </div>

        <div class="text-center mt-4 mb-4" style="max-width: 140px; max-height: 140px;">
            <img src="//placehold.it/140?text=IMAGE" class="img-fluid" id="preview"/>
        </div>

        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <label class="input-group-text">Обложка</label>
            </div>

            <div class="custom-file">
                <input type="file" name="image" class="custom-file-input" id="image">
                <label class="custom-file-label custom-file-label-image" for="image">Choose file</label>
            </div>
        </div>

        {{--<div class="card mb-4">--}}
        {{--<h5 class="card-title card-header">#Добавить хештеги ({{$hashtags->count()}})</h5>--}}
        {{--<ul class="list-group list-group-flush">--}}
        {{--<li class="list-group-item d-flex">--}}
        {{--@foreach($hashtags as $hashtag)--}}
        {{--<div class="custom-control custom-checkbox">--}}
        {{--<input type="checkbox" value={{$hashtag->id}}  class="custom-control-input"--}}
        {{--name="hashtags[]" id="{{$hashtag->hashtag}}">--}}
        {{--<label class="custom-control-label mr-2"--}}
        {{--for="{{$hashtag->hashtag}}">{{$hashtag->hashtag}}</label>--}}
        {{--</div>--}}
        {{--@endforeach--}}
        {{--</li>--}}
        {{--</ul>--}}
        {{--</div>--}}

        <button type="submit" class="btn btn-primary mb-4">Сохранить</button>
    </form>

@endsection

<script>
    document.addEventListener("DOMContentLoaded", function (event) {
        // $(document).ready(function () {
        //     setTimeout(function () {
        //         CKEDITOR.replace('text');
        //     }, 100);
        // });

        $(document).on('change', '#image', function () {
            let input = $(this)[0];
            if (input.files && input.files[0]) {
                let reader = new FileReader();
                reader.onload = function (e) {
                    $('#preview').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
                $(".custom-file-label-image").text($(this).get(0).files[0].name);
            }
        });

        $(document).on('change', '#video', function () {
            $('#video').on('change', function (e) {
                $(".custom-file-label-video").text($(this).get(0).files[0].name);
            });
        });

    });


    // document.addEventListener("DOMContentLoaded", function (event) {
    //
    //     $('#video').on('change',function(e){
    //         $(this).next('.custom-file-label').html(e.target.files[0].name);
    //     });
    //     $('#image').on('change',function(e){
    //         $(this).next('.custom-file-label').html(e.target.files[0].name);
    //     })
    // });

</script>

