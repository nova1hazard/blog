@extends('layouts.admin')

@section('admin-content')
    @if (session('status'))
        <div class="alert alert-success mb-4">
            {{ session('status') }}
        </div>
    @endif

    <art-category></art-category>
    <v-art-crud></v-art-crud>

    <a href="{{route('admin.art.create')}}" class="btn btn-primary mb-4" role="button"
       aria-pressed="true"><i class="fas fa-plus-square mr-2"></i>Добавить рисунки (мультизагрузка)
    </a>
@endsection