@extends('layouts.admin')

@section('admin-content')

    <h2 class="bd-title mb-4">Загрузка артов</h2>
    @csrf
    <art-uploader></art-uploader>
@endsection
