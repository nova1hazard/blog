@extends('layouts.admin')

@section('admin-content')

    <h2 class="bd-title mb-4">Загрузка фото</h2>
    @csrf
    <dj-uploader></dj-uploader>

@endsection
