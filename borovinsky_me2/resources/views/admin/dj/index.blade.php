@extends('layouts.admin')

@section('admin-content')
    @if (session('status'))
        <div class="alert alert-success mb-4">
            {{ session('status') }}
        </div>
    @endif

    <dj-category></dj-category>
    <v-dj-crud></v-dj-crud>

    <a href="{{route('admin.dj.create')}}" class="btn btn-primary mb-4" role="button"
       aria-pressed="true">
        <i class="fas fa-plus-square mr-2"></i>
        Добавить фото (мультизагрузка)
    </a>
@endsection