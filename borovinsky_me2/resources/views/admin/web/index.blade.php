@extends('layouts.admin')

@section('admin-content')
    @if (session('status'))
        <div class="alert alert-success mb-4">
            {{ session('status') }}
        </div>
    @endif

    <web-category></web-category>

    <div class="list-group mb-4 card">
                    <span class="list-group-item list-group-item-action active">
                        Веб-проекты ({{$webs->count()}})
                    </span>
        <div class="card-body">
            <table class="table table-sm">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Заголовок</th>
                    <th scope="col">Текст</th>
                    <th scope="col">Категория</th>
                    <th scope="col">Дата создания проекта</th>
                    <th scope="col">Оригинальная ссылка</th>
                    <th scope="col" class="text-right">Действия</th>
                </tr>
                </thead>
                <tbody>
                @foreach($webs as $k=>$web)

                    <tr>
                        <th scope="row">{{$k+1}}</th>
                        <td><b>{{$web->title}}</b></td>
                        <td> {{ \Illuminate\Support\Str::limit($web->text, 30) }}...</td>
                        <td>{{$web->category['category']}}</td>
                        <td>{{$web->project_date}}</td>
                        <td>{{$web->original_link}}</td>
                        <td class="text-right">
                            <a href="/admin/web/{{$web->id}}" class="btn btn-primary" role="button"
                               aria-pressed="true"><i class="fas fa-edit"></i></a>
                            <a href="/admin/web/{{$web->id}}" class="btn btn-danger" role="button"
                               onclick="event.preventDefault();
                        document.getElementById('delete-form').submit();">
                                <i class="fas fa-minus-square"></i>
                            </a>
                            <form id="delete-form" action="/admin/web/{{$web->id}}" method="POST"
                                  style="display: none;">
                                <input name="_method" type="hidden" value="DELETE">
                                @csrf
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <a href="/admin/web/create" class="btn btn-primary mb-4" role="button"
       aria-pressed="true"><i class="fas fa-plus-square mr-2"></i>Добавить проект
    </a>
@endsection