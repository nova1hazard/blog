@extends('layouts.admin')

@section('admin-content')

    <h2 class="bd-title mb-4">Редактирвание статьи "{{$web->title}}"</h2>
    <form action="/admin/web/{{$web->id}}" method="POST">
        @method('PATCH')
        @csrf

        <div class="row mb-3">
            <div class="col-md-4">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <label class="input-group-text">Заголовок</label>
                    </div>
                    <input name="title" class="form-control" id="title" value="{{$web->title}}">
                </div>
            </div>
            <div class="col-md-4">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <label class="input-group-text">Дата проекта</label>
                    </div>
                    <input type="date" value="{{$web->project_date}}" class="form-control mydate" name="project_date" placeholder="Дата"
                           id="project_date">
                </div>
            </div>
            <div class="col-md-4">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <label class="input-group-text">Категория</label>
                    </div>
                    <select class="custom-select" name="category_id">
                        @foreach($web_categories as $category)
                            <option {{$web->category['category']==$category->category?'selected':""}} value="{{$category->id}}">{{$category->category}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
        <div class="row mb-3">
            <div class="col-md-4">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <label class="input-group-text">Оригинальная ссылка</label>
                    </div>
                    <input name="original_link" class="form-control" id="original_link" value="{{$web->original_link}}">
                </div>
            </div>
        </div>

        <div class="mb-4">
            <textarea name="text">{!! $web->text !!}</textarea>

        </div>

        <button type="submit" class="btn btn-primary mb-4 mt-4">Сохранить</button>
    </form>

    <script src="{{ asset('assets/js/tinyMCE.js')}}"></script>
@endsection



