@extends('layouts.admin')

@section('admin-content')

    <h2 class="bd-title mb-4">Новый веб-проект</h2>

    <form action="/admin/web" method="POST">
        @csrf

        <div class="row mb-3">
            <div class="col-md-4">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <label class="input-group-text">Заголовок</label>
                    </div>
                    <input name="title" class="form-control" id="title">
                </div>
            </div>
            <div class="col-md-4">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <label class="input-group-text">Дата проекта</label>
                    </div>
                    <input type="date" class="form-control mydate" name="project_date" placeholder="Дата"
                           id="project_date">
                </div>
            </div>
            <div class="col-md-4">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <label class="input-group-text">Категория</label>
                    </div>
                    <select class="custom-select" name="category_id">
                        @foreach($web_categories as $category)
                            <option value="{{$category->id}}">{{$category->category}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
        <div class="row mb-3">
            <div class="col-md-4">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <label class="input-group-text">Оригинальная ссылка</label>
                    </div>
                    <input name="original_link" class="form-control" id="original_link">
                </div>
            </div>
        </div>

        <textarea name="text"></textarea>
        {{--<input name="image" type="file" id="upload" class="hidden" onchange="" style="display: none;">--}}


        <button type="submit" class="btn btn-primary mb-4 mt-4">Сохранить</button>
    </form>

    <script src="{{ asset('assets/js/tinyMCE.js')}}"></script>
@endsection



