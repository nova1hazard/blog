@extends('layouts.admin')

@section('admin-content')
    @if (session('status'))
        <div class="alert alert-success mb-4">
            {{ session('status') }}
        </div>
    @endif

    <music-genres></music-genres>

    <v-music-crud></v-music-crud>
@endsection