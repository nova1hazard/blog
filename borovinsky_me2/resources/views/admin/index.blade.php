@extends('layouts.admin')

@section('admin-content')

    <a href="/admin/content" class="btn btn-light mb-3" role="button" aria-pressed="true">
        <i class="fas fa-align-center mr-2"></i>ТЕКСТОВЫЙ КОНТЕНТ</a>

    <a href="/admin/portfolio" class="btn btn-light mb-3" role="button" aria-pressed="true">
        <i class="fas fa-atom mr-2"></i>ПРОЕКТЫ ПОРТФОЛИО</a>

    <a href="/admin/users" class="btn btn-light mb-3" role="button" aria-pressed="true">
        <i class="fas fa-user mr-2"></i>Пользователи</a>

    <ul class="list-group mb-4">
        <span class="list-group-item list-group-item-action active">
            Портфолио (содержимое проектов) [ /portfolio ]
        </span>
        @foreach ($admin_sections as $section)
            <li class="list-group-item d-flex justify-content-between align-items-center">
                <div>
                    @if($section->link == 'article')
                        <span class="badge badge-primary badge-pill mr-4">{{$articles_сount}}</span>
                        <a href="{{route('articles')}}" target="_blank"><i class="text-secondary fas fa-cog mr-2"></i></a>
                    @endif
                    @if($section->link == 'art')
                        <span class="badge badge-primary badge-pill mr-4">{{$arts_сount}}</span>
                        <a href="{{route('arts')}}" target="_blank"><i class="text-secondary fas fa-cog mr-2"></i></a>
                    @endif
                    @if($section->link == 'web')
                        <span class="badge badge-primary badge-pill mr-4">{{$webs_сount}}</span>
                        <a href="{{route('web-projects')}}" target="_blank"><i class="text-secondary fas fa-cog mr-2"></i></a>
                    @endif
                    @if($section->link == 'music')
                        <span class="badge badge-primary badge-pill mr-4">{{$musics_сount}}</span>
                        <a href="{{route('music')}}" target="_blank"><i class="text-secondary fas fa-cog mr-2"></i></a>
                    @endif
                    @if($section->link == 'dj')
                        <span class="badge badge-primary badge-pill mr-4">{{$djs_сount}}</span>
                        <a href="{{route('dj-career')}}" target="_blank"><i class="text-secondary fas fa-cog mr-2"></i></a>
                    @endif
                    @if($section->link == 'video')
                        <span class="badge badge-primary badge-pill mr-4">{{$videos_сount}}</span>
                        <a href="{{route('video-projects')}}" target="_blank"><i class="text-secondary fas fa-cog mr-2"></i></a>
                    @endif
                    {{$section->section}}</div>
                <div>
                    {{--<a href="#" class="btn btn-primary" role="button" aria-pressed="true">Скрыть</a>--}}
                    <a href="/admin/{{$section->link}}" class="btn btn-primary" role="button" aria-pressed="true"><i
                                class="fas fa-edit"></i></a>
                </div>
            </li>
        @endforeach
    </ul>
    {{--<div class="row mb-4">--}}
    {{--<div class="col-md-6">--}}
    {{--<div class="card">--}}
    {{--<h5 class="card-header list-group-item list-group-item-action active">Header</h5>--}}
    {{--<div class="card-body">--}}
    {{--<form action="/admin/header" method="POST">--}}
    {{--@csrf--}}
    {{--<div class="form-group">--}}
    {{--<label for="header" class="mb-4">Order</label>--}}
    {{--@foreach($header_links as $i=>$link)--}}
    {{--<div class="input-group mb-2">--}}
    {{--<div class="input-group-prepend">--}}
    {{--<span class="input-group-text" id="">{{$i+1}}</span>--}}
    {{--</div>--}}
    {{--<input type="hidden" name="menu[{{$i}}][order]" class="form-control"--}}
    {{--value="{{$i+1}}">--}}
    {{--<input type="text" name="menu[{{$i}}][section]"--}}
    {{--value="{{$header[$i]->section ?? ""}}" class="form-control">--}}
    {{--<select class="custom-select" type="text" name="menu[{{$i}}][link_id]">--}}
    {{--<option selected--}}
    {{--value="{{$header[$i]['link']['id'] ?? ""}}">{{$header[$i]['link']['link'] ?? ""}}</option>--}}
    {{--@forelse($header_links as $k=>$link)--}}
    {{--<option value="{{$link->id}}">{{$link->link}}</option>--}}
    {{--@empty ""--}}
    {{--@endforelse--}}
    {{--</select>--}}
    {{--<input type="text" name="menu[{{$i}}][link]" value="{{$header[$i]->link ?? ""}}"--}}
    {{--class="form-control">--}}
    {{--</div>--}}
    {{--@endforeach--}}
    {{--<textarea class="form-control" name="header" id="header" rows="5"></textarea>--}}
    {{--<button type="submit" class="btn btn-primary mt-2" disabled>Сохранить</button>--}}
    {{--</div>--}}
    {{--</form>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--<div class="col-md-6">--}}
    {{--<div class="card">--}}
    {{--<h5 class="card-header list-group-item list-group-item-action active">Footer</h5>--}}
    {{--<div class="card-body">--}}
    {{--<form>--}}
    {{--<div class="form-group">--}}
    {{--<label for="footer" class="mb-2">Footer</label>--}}
    {{--<textarea class="form-control" id="footer" rows="5"></textarea>--}}
    {{--<button type="submit" class="btn btn-primary mt-4">Сохранить</button>--}}
    {{--</div>--}}
    {{--</form>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
@endsection

<style>
    .small-prev-img {
        max-width: 100px !important;
    }
</style>