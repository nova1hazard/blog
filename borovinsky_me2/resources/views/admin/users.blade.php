@extends('layouts.admin')

@section('admin-content')
    <div class="card">
        <span class="list-group-item list-group-item-action active">
            Пользователи ({{count($users)}})
        </span>
        <div class="card-body">
            <table class="table table-sm">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Ник</th>
                    <th scope="col">Почта</th>
                    <th scope="col">Роль</th>
                    <th scope="col">Создан</th>
                </tr>
                </thead>
                <tbody>
                @forelse ($users as $k=>$user)
                    <tr>
                        <th scope="row">{{$k+1}}</th>
                        <td>{{$user['name']}}</td>
                        <td>{{$user['email']}}</td>
                        <td>@foreach($user['roles'] as $role)
                                {{$role['role']}}
                            @endforeach</td>
                        <td>{{$user['created_at']}}</td>
                    </tr>
                @empty
                    <li class="list-group-item d-flex justify-content-between align-items-center">Нет юзеров</li>
                @endforelse
                </tbody>
            </table>
        </div>
    </div>
    <br>
    <a href="/admin" class="btn btn-link float-right" role="button">Назад</a>
@endsection

<style>
    .small-prev-img {
        max-width: 100px !important;
    }
</style>