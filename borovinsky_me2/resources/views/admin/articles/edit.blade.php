@extends('layouts.admin')

@section('admin-content')

    <h2 class="bd-title mb-4">Редактирвание статьи "{{$article->title}}"</h2>
    <form action="/admin/article/{{$article->id}}" method="POST">
        @method('PATCH')
        @csrf

        <div class="row mb-3">
            <div class="col-md-6">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <label class="input-group-text">Заголовок</label>
                    </div>
                    <input name="title" class="form-control" id="title" value="{{$article->title}}">
                </div>
            </div>
            <div class="col-md-6">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <label class="input-group-text">Категория</label>
                    </div>
                    <select class="custom-select" name="category_id">
                        @foreach($articles_categories as $category)
                            <option {{$article->category['category']==$category->category?'selected':""}} value="{{$category->id}}">{{$category->category}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>

        <div class="mb-4">
            <textarea name="text">
                {{$article->text}}
            </textarea>
        </div>
        <div class="card mb-4">
            <h5 class="card-title card-header">#Добавить хештеги ({{$hashtags->count()}})</h5>
            <ul class="list-group list-group-flush">
                <li class="list-group-item d-flex">
                    @foreach($hashtags as $hashtag)
                        <div class="custom-control custom-checkbox">
                            {{--костыль проверка на выбранные значения массива)--}}
                            @php
                                $checked = false;
                                $hashtags_selected = $article->hashtag->toArray();
                                foreach ($hashtags_selected as $hashtag_selected) {
                                    if($hashtag_selected['hashtag'] == $hashtag->hashtag){
                                        $checked = true;
                                        break;
                                    }
                                }
                            @endphp
                            <input {{$checked==true?'checked':""}} type="checkbox" value={{$hashtag->id}}  class="custom-control-input"
                                       name="hashtags[]" id="{{$hashtag->hashtag}}">
                            <label class="custom-control-label mr-2"
                                   for="{{$hashtag->hashtag}}">{{$hashtag->hashtag}}</label>
                        </div>
                    @endforeach
                </li>
            </ul>
        </div>

        <button type="submit" class="btn btn-primary mb-4">Сохранить</button>
    </form>

    <script src="{{ asset('assets/js/tinyMCE.js')}}"></script>


@endsection