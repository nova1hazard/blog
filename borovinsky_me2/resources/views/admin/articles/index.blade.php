@extends('layouts.admin')

@section('admin-content')
    @if (session('status'))
        <div class="alert alert-success mb-4">
            {{ session('status') }}
        </div>
    @endif

    <article-category></article-category>
    <article-hashtags></article-hashtags>

    <div class="list-group mb-4 card">
                    <span class="list-group-item list-group-item-action active">
                        Статьи ({{$articles->count()}})
                    </span>
      <div class="card-body">
        <table class="table table-sm">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Заголовок</th>
                <th scope="col">Текст</th>
                <th scope="col">Категория</th>
                <th scope="col">Хештеги</th>
                <th scope="col">Дата изменения</th>
                <th scope="col" class="text-right">Действия</th>
            </tr>
            </thead>
            <tbody>
            @foreach($articles as $k=>$article)

                <tr>
                    <th scope="row">{{$k+1}}</th>
                    <td><b>{{$article->title}}</b></td>
                    <td> {{\Illuminate\Support\Str::limit($article->text, 30)}}...</td>
                    <td>{{$article->category['category']}}</td>
                    <td>
                        <ul>
                        @forelse($article->hashtag as $hashtag)
                            <li>{{$hashtag['hashtag']}}</li>
                            @empty
                        @endforelse
                        </ul>
                    </td>
                    <td>{{$article->updated_at}}</td>
                    <td class="text-right">
                        <a href="/admin/article/{{$article->id}}/edit" class="btn btn-primary" role="button"
                           aria-pressed="true"><i class="fas fa-edit"></i></a>
                        <a href="/admin/article/{{$article->id}}" class="btn btn-danger" role="button"
                           onclick="event.preventDefault();
                        document.getElementById('delete-form').submit();">
                            <i class="fas fa-minus-square"></i>
                        </a>
                        <form id="delete-form" action="/admin/article/{{$article->id}}" method="POST"
                              style="display: none;">
                            <input name="_method" type="hidden" value="DELETE">
                            @csrf
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
      </div>
    </div>
    <a href="/admin/article/create" class="btn btn-primary mb-4" role="button"
       aria-pressed="true"><i class="fas fa-plus-square mr-2"></i>Добавить статью
    </a>
@endsection