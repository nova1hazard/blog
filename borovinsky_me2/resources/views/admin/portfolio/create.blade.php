@extends('layouts.admin')

@section('admin-content')

    <h2 class="bd-title mb-4">Новый проект</h2>
    <form action="/admin/portfolio" method="POST" enctype="multipart/form-data">
        @csrf

        <div class="row mb-3">
            <div class="col-md-4">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <label class="input-group-text">Заголовок</label>
                    </div>
                    <input name="title" class="form-control" id="title">
                </div>
            </div>
            <div class="col-md-4">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <label class="input-group-text">Ссылка</label>
                    </div>
                    <select class="custom-select" name="link" id="link">
                        @foreach($header_links as $link)
                            <option value="{{$link->link}}">{{$link->link}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-4">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <label class="input-group-text">Порядок (приоритет вывода)</label>
                    </div>
                    <input name="order" class="form-control" id="order">
                </div>
            </div>
        </div>


        <div class="text-center mt-4 mb-4" style="max-width: 140px; max-height: 140px;">
            <img src="//placehold.it/140?text=IMAGE" class="img-fluid" id="preview" />
        </div>

        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <label class="input-group-text">Обложка</label>
            </div>
            <div class="custom-file">
                <input type="file" name="image" class="custom-file-input" id="image">
                <label class="custom-file-label" for="image">Choose file</label>
            </div>
        </div>

        <div class="mb-4">
            <label for="ta1">Описание</label>
            <textarea name="text" class="form-control mt-2" id="ta1">
            </textarea>
        </div>

        <button type="submit" class="btn btn-primary mb-4">Сохранить</button>
    </form>

@endsection
<script>
    document.addEventListener("DOMContentLoaded", function (event) {

        $(document).on('change','#image' , function(){
            let input = $(this)[0];
            if (input.files && input.files[0]) {
                let reader = new FileReader();
                reader.onload = function (e) {
                    $('#preview').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
                $(".custom-file-label").text($(this).get(0).files[0].name);
            }
        });
    });
</script>

