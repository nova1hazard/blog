@extends('layouts.admin')

@section('admin-content')
    @if (session('status'))
        <div class="alert alert-success mb-4">
            {{ session('status') }}
        </div>
    @endif

    <div class="card">
        <span class="list-group-item list-group-item-action active">
            Портфолио ({{$portfolios->count()}})
        </span>
        <div class="card-body">
            <table class="table table-sm">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Обложка</th>
                    <th scope="col">Заголовок</th>
                    <th scope="col">Текст</th>
                    <th scope="col">Порядок</th>
                    <th scope="col">Ссылка</th>
                    <th scope="col" class="text-right">Действия</th>
                </tr>
                </thead>
                <tbody>
                @forelse ($portfolios as $k=>$portfolio)
                    <tr>
                        <th scope="row">{{$k+1}}</th>
                        <td><img src="/storage/{{$portfolio->image}}" class="img-fluid small-prev-img"></td>
                        <td><b>{{$portfolio->title}}</b></td>
                        <td> {!! \Illuminate\Support\Str::limit($portfolio->text, 30)!!}...</td>
                        <td>{{$portfolio->order}}</td>
                        <td>{{$portfolio->link}}</td>
                        <td class="text-right">
                            <div>
                                <a href="/admin/portfolio/{{$portfolio->id}}/edit" class="btn btn-primary" role="button"
                                   aria-pressed="true"><i class="fas fa-edit"></i></a>
                                <a href="/admin/portfolio/rm/{{$portfolio->id}}" class="btn btn-danger" role="button">
                                    <i class="fas fa-minus-square"></i>
                                </a>
                            </div>
                        </td>
                    </tr>
                @empty
                    <li class="list-group-item d-flex justify-content-between align-items-center">Нет проектов</li>
                @endforelse
                </tbody>
            </table>
        </div>
    </div>
    <div class="form-group">
        <a href="{{route('admin.portfolio.create')}}">
            <button type="submit" class="btn btn-primary mt-4">Добавить портфолио</button>
        </a>
    </div>
@endsection

<style>
    .small-prev-img {
        max-width: 100px !important;
    }
</style>