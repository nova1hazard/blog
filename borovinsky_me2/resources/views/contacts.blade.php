<a id="Contacts"></a>
<div class="contacts">
    <div class="container">
        <div class="plr">
            <form id="em_send">
                <div class="form-row">
                    <div class="col-md-3">
                        <span class="leave_message">@lang("global.send_message")</span>
                        <input type="text" class="form-control mt_22" id="send_name" placeholder="@lang("global.name")" required>

                        <input type="text" class="form-control mt_22" id="send_email" placeholder="@lang("global.email")*" required>

                        <input type="text" class="form-control mt_22" id="send_subject" placeholder="@lang("global.subject")" required>

                    </div>
                    <div class="col-md-3 text-right">
                        <div class="form-group">
                            <textarea class="form-control" id="FormControlTextarea1" rows="6" placeholder="@lang("global.message")"
                                      required></textarea>

                        </div>
                        <meta name="csrf-token" content="{{ csrf_token() }}">
                        <button type="button" id="add_comment_button" class="btn btn-danger hire_me">@lang("global.send_message")
                        </button>
                    </div>
            </form>
            <div class="col-md-5 offset-md-1">
                <div class="container">
                    <div class="contact_holder mt_104">
                        <div class="icon_wrap">
                            <img src="{{asset('assets/images/icons/house.svg')}}" alt="Address">
                        </div>

                        <div class="contact_info">@lang("global.location")<br>
                            <div class="contacts_small_text">@lang("global.address")</div>
                        </div>
                    </div>
                    <div class="contact_holder">
                        <div class="icon_wrap">
                            <img src="{{asset('assets/images/icons/paper-plane.svg')}}" alt="Email">
                        </div>

                        <div class="contact_info">
                            @lang("global.email")<br>
                            <div class="contacts_small_text"><a href="mailto:nova1hazard@gmail.com">nova1hazard@gmail.com</a></div>
                        </div>
                    </div>
                    <div class="contact_holder">
                        <div class="icon_wrap">
                            <img src="{{asset('assets/images/icons/smartphone.svg')}}" alt="phone">
                        </div>

                        <div class="contact_info">
                            @lang("global.phone")<br>
                            <div class="contacts_small_text"><a href="tel:+79137833910">+7 913 783 39 10</a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script>

    //-------------------Add Comment

    $(function () {


        $('#add_comment_button').on('click', function () {

            send_name = document.getElementById('send_name').value.replace(/<[^>]+>/g, '');
            send_subject = document.getElementById('send_subject').value.replace(/<[^>]+>/g, '');
            send_text = document.getElementById('FormControlTextarea1').value.replace(/<[^>]+>/g, '');
            send_email = document.getElementById('send_email').value;

            var errors = [];

            if (send_name === '') {
                errors.push("enter your name (30 symbols max)");

            }

            if (send_name.length > 30) {
                errors.push("don't be a flooder! 30 symbols is enough for name!");

            }

            if (send_text === '') {
                errors.push("enter your message (1000 symbols is max)");
            }

            if (send_text.length > 1000) {
                errors.push("don't be flooder! 1000 symbols is enough for message!");
            }


            if (send_email === '') {
                errors.push("enter your e-mail address for response");
            }

//check mail

            adr = send_email;
            adr_pattern = /[0-9a-z_]+@[0-9a-z_]+\.[a-z]{2,5}/i;
            prov = adr_pattern.test(adr);

            if (prov == false) {
                errors.push("fill e-mail field correctly like (google@gmail.com)");
            }


            if (errors.length == 0) {


                $.ajax({

                    url: '{{ route('send_email') }}',

                    type: "POST",


                    data: {

                        'send_name': send_name,
                        'send_subject': send_subject,
                        'send_text': send_text,
                        'send_email': send_email
                    },

                    headers: {

                        'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')

                    },

                    success: function (data) {


                        swal({
                            title: "Thanks for your message.",
                            text: "i will definitely read it!",
                            icon: "success",
                            button: "OK",
                        });

                    },

                    error: function (msg) {

                        swal("Error", 'Data translation error', "error");

                    }

                });

            }
            else {
                swal("Error", 'Please ' + errors[0] + '!', "error");
            }

        });

    })

</script>