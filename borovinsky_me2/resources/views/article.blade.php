@forelse($articles as $article)
<div class="article">
    <div class="article__header">
        <h2 class="bold-title">{{$article->title}}</h2>
        Категория: ({{$article->category['category']}})
        <div class="article__author">
            <img class="article__avatar" src="assets/doe.png">
            <span class="article__author-text"><i class="text_grey-lighten">By</i> <b>John Doe</b> <i
                        class="text_grey-lighten">In</i> <b>Business</b> <i class="text_grey-lighten">Posted</i> <b>May 24, 2013</b></span>
        </div>
        <div class="share-in-socials">
            <div class="share-in-socials__item share-in-socials__item_google">
                <div class="share-in-socials__icon share-in-socials__icon_google"></div>
                <div class="share-in-socials__item_text">
                    Share on Google +
                </div>
            </div>
            <div class="share-in-socials__item share-in-socials__item_fb">
                <div class="share-in-socials__icon share-in-socials__icon_fb"></div>
                <div class="share-in-socials__item_text">
                    Share on Facebook
                </div>
            </div>
            <div class="share-in-socials__item share-in-socials__item_twitter">
                <div class="share-in-socials__icon share-in-socials__icon_twitter"></div>
                <div class="share-in-socials__item_text">
                    Share on Twitter
                </div>
            </div>
            <div class="share-in-socials__item share-in-socials__item_whats-app">
                <div class="share-in-socials__icon share-in-socials__icon_whatsapp"></div>
                <div class="share-in-socials__item_text">
                    Share on Whatsapp
                </div>
            </div>
        </div>
    </div>
    <div class="article__body">
        {!! $article->text !!}
        {{--orem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et--}}
        {{--dolore--}}
        {{--magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea--}}
        {{--commodo--}}
        {{--consequat.--}}
        {{--Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et--}}
        {{--dolore--}}
        {{--magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea--}}
        {{--commodo--}}
        {{--consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla--}}
        {{--pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id--}}
        {{--est--}}
        {{--laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium,--}}
        {{--totam--}}
        {{--rem aperiam, eaque ipsa quae ab illo inventore veritatis.--}}
        {{--Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et--}}
        {{--dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex--}}
        {{--ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat--}}
        {{--nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit--}}
        {{--anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque--}}
        {{--laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae--}}
        {{--dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia--}}
        {{--consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem--}}
        {{--ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut--}}
        {{--labore et dolore magnam aliquam quaerat voluptatem.--}}
        {{--<div class="article__img-container">--}}
            {{--<img src="assets/blog.png" alt="img">--}}
            {{--<div class="article__img-description">--}}
                {{--<b>How to disappear completely and never be found</b><br>--}}
                {{--<i>Photo copyright Jason Gilespy</i>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore--}}
        {{--magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo--}}
        {{--consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla--}}
        {{--pariatur.--}}
        {{--<div class="article__blockquote">--}}
            {{--<hr>--}}
            {{--<div class="article__blockquote-text">--}}
                {{--“Train yourself to let go of--}}
                {{--everything you fear to lose.”--}}
            {{--</div>--}}
            {{--<div class="article__blockquote-author">--}}
                {{--Master Yoda--}}
            {{--</div>--}}
            {{--<hr>--}}
        {{--</div>--}}
        {{--Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore--}}
        {{--magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo--}}
        {{--consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla--}}
        {{--pariatur.--}}

    </div>
    <div class="article__tag-area">
        @forelse($article->hashtag as $hashtag)
            <div class="article__tag">
                {{$hashtag['hashtag']}}
            </div>
            @empty
        @endforelse
    </div>
    <hr>
</div>
    @empty
    Статей ещё нет
@endforelse