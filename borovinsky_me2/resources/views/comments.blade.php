<meta charset="utf-8">
<div class="container">
    <div class="plr">
        <div class="row">
            <div class="col-md-12">
      <span class="block_header mt_t fr">


       <span id="star_line"></span>
          {{$total_rate}}
      </span>
            </div>
        </div>
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <div class="btn btn-dark" id="button_comments" onclick="ToggleClick(this)"><i class="inselector"><i class="inselector fa fa-sort-up" id="arrow"></i></i></div>
        <div id="comments_wrap">
            <div class="row com_pointer">


                @foreach ($all_comments as $k =>  $comment)

                    @php
                        $str_time = strtotime($comment->time);
                        $str_time = date("F jS Y", $str_time);
                       //{{mb_convert_encoding($comment->text, "Windows-1251", "UTF-8")}}
                    @endphp

                    <div class="comment_inner com{{$k}}">
                        <div class="col-md-12">
                            {!! Html::image(('assets/images/avatar/'.$comment->avatar.'.png'),'',['class' => 'avatar','alt'=> 'avatar']) !!}
                            <div class="com_nick">{{ $comment->name}} {{$comment->rate}}/5<span>{{$str_time}}</span></div>
                            <div class="com_text">{{$comment->text}}</div>
                        </div>
                    </div>

                @endforeach
            </div>
        </div>

        <div class="col-md-4 offset-md-1">
            <span id="Show_all" onclick="ShowAllComm()"> Show all comments <i class="far fa-eye"></i></span>
        </div>



        <div class="submit_comment">
            <form id="com_validate">
                <div class="form-row">
                    <div class="col-md-6">
                        <input type="text" class="form-control" id="CommentName" placeholder="Name*" required>
                        <div class="invalid-feedback">Enter your name please.</div>

                        <div class="form-group">
                            <textarea class="form-control" id="FormControlTextarea2" rows="6" placeholder="Comment*" required></textarea>
                            <div class="invalid-feedback">Enter your comment please.</div>
                        </div>

                        <div class="form-group">
                            <select class="custom-select" id="rate" required>
                                <option value="">Please rate this division</option>
                                <option value="5">5</option>
                                <option value="4">4</option>
                                <option value="3">3</option>
                                <option value="2">2</option>
                                <option value="1">1</option>
                            </select>
                            <div class="invalid-feedback">Select a rating and share your opinion!</div>
                        </div>
                        <button type="button" id="Add_comment_button" class="btn btn-dark sub_comment_btn">Submit</button>
                    </div>
                </div>
        </div>
    </div>
</div>



<div class="comments">
    <div class="comments__total">
        2 comments
    </div>
    <form action="{{ route('addComment') }}" method="POST">
        @csrf
        <div class="comments__post-a-comment">
            <div class="comments__author-wrapper">
                <img class="comments__avatar" src="assets/no-avatar.png" alt="user-avatar">
                <div class="comments__author-container-wrapper">
                    <div class="comments__author-container">
                        <div class="comments__author">John Doe</div>
                        ·
                        <div class="comments__date">Right now</div>
                    </div>
                    <textarea class="input" name="text" cols="30" rows="10"></textarea>
                    <button type="submit" class="comments__post-button action-button">Post</button>
                </div>
            </div>
        </div>
    </form>
    @foreach($comments as $k=>$comment)
        <div class="comments__comment">
            <div class="comments__author-wrapper">
                <img class="comments__avatar" src="assets/no-avatar.png" alt="user-avatar">
                <div class="comments__author-container-wrapper">
                    <div class="comments__author-container">
                        <div class="comments__author">{{$comment->owner->name}}</div>
                        ·
                        <div class="comments__date">{{$comment->created_at}} </div>
                        {{--12/04/2015 at 12:45--}}
                    </div>
                    <div class="comments__text">{{$comment->text}}
                    </div>
                    <div class="comments__actions">
                            <span class="comments__actions-chevron-container">
                                <i class="comments__actions-button fas fa-chevron-up"></i>
                                |
                                <i class="comments__actions-button fas fa-chevron-down"></i>
                            </span>
                        <a href="" class="comments__actions-button_reply">Reply</a>
                        <a href="" class="comments__actions-button_share">Share      <i class="fas fa-chevron-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
    {{--<div class="comments__comment comments__reply">--}}
    {{--<div class="comments__author-wrapper">--}}
    {{--<img class="comments__avatar" src="assets/no-avatar.png" alt="user-avatar">--}}
    {{--<div class="comments__author-container-wrapper">--}}
    {{--<div class="comments__author-container">--}}
    {{--<div class="comments__author">John Doe</div>--}}
    {{--·--}}
    {{--<div class="comments__date">12/04/2015 at 12:45</div>--}}
    {{--</div>--}}
    {{--<div class="comments__text">One of the details that 'A Christmas Story' gets right is the threat of--}}
    {{--having your--}}
    {{--mouth washed out with Lifebuoy soap. Not any soap. Lifebuoy.--}}
    {{--</div>--}}
    {{--<div class="comments__actions">--}}
    {{--<span class="comments__actions-chevron-container">--}}
    {{--<i class="comments__actions-button fas fa-chevron-up"></i>--}}
    {{--|--}}
    {{--<i class="comments__actions-button fas fa-chevron-down"></i>--}}
    {{--</span>--}}
    {{--<a href="" class="comments__actions-button_reply">Reply</a>--}}
    {{--<a href="" class="comments__actions-button_share">Share      <i class="fas fa-chevron-right"></i></a>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
</div>
