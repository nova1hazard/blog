@extends('layouts.app')

@section('content')
    <meta name="description" content="Мои рисунки">
    <a id="project"></a>
    <div class="container">
        <div class="plr">
                @forelse($all_arts as $art_cat)
                    <span class="block_header mt_t2">{{$art_cat['category']}} ({{count($art_cat['art'])}})
                        </span>
                    <div class="line">
                        </div>
                    <div class="project_text" style="margin-bottom: 0px">
                        {{$art_cat['text']}}
                    </div>
                    <gallery :data='@json($art_cat['art'])'></gallery>
                    {{--<div class="gallery flexed">--}}
                        {{--@foreach($art_cat['art'] as $art)--}}
                            {{--<a href="/storage/{{$art['image']}}" data-caption='{{$art['text']}}'>--}}
                                {{--<img src="/storage/{{$art['image_sm']}}" alt="gallery image" class='gal_img'>--}}
                            {{--</a>--}}
                        {{--@endforeach--}}
                    {{--</div>--}}
                @empty
                @endforelse

        </div>
    </div>

@endsection
