<?php
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs;
// Admin
Breadcrumbs::for('admin.', function ($trail) {
    $trail->push('admin', '/admin');
});

// Admin > Article
Breadcrumbs::for('admin.article.index', function ($trail) {
    $trail->parent('admin.');
    $trail->push('article', route('admin.article.index'));
});

// Admin > Article > Create
Breadcrumbs::for('admin.article.create', function ($trail) {
    $trail->parent('admin.');
    $trail->push('article', route('admin.article.index'));
    $trail->push('create', route('admin.article.create'));
});

// Admin > Article > Edit > [$article_id]
Breadcrumbs::for('admin.article.edit', function ($trail,$article_id) {
    $trail->parent('admin.');
    $trail->push('article', route('admin.article.index'));
    $trail->push($article_id, route('admin.article.edit', $article_id));
    $trail->push('edit', "");
});

// Admin > Art
Breadcrumbs::for('admin.art.index', function ($trail) {
    $trail->parent('admin.');
    $trail->push('art', route('admin.art.index'));
});

// Admin > Art > Create
Breadcrumbs::for('admin.art.create', function ($trail) {
    $trail->parent('admin.');
    $trail->push('art', route('admin.art.index'));
    $trail->push('create', route('admin.art.create'));
});

// Admin > Art > Edit > [$art_id]
Breadcrumbs::for('admin.art.edit', function ($trail,$art_id) {
    $trail->parent('admin.');
    $trail->push('art', route('admin.art.index'));
    $trail->push($art_id, route('admin.art.edit', $art_id));
    $trail->push('edit', "");
});

// Admin > Web
Breadcrumbs::for('admin.web.index', function ($trail) {
    $trail->parent('admin.');
    $trail->push('web', route('admin.web.index'));
});

// Admin > Web > Create
Breadcrumbs::for('admin.web.create', function ($trail) {
    $trail->parent('admin.');
    $trail->push('web', route('admin.web.index'));
    $trail->push('create', route('admin.web.create'));
});

// Admin > Web > Edit > [$web_id]
Breadcrumbs::for('admin.web.edit', function ($trail,$id) {
    $trail->parent('admin.');
    $trail->push('web', route('admin.web.index'));
    $trail->push($id, route('admin.web.edit', $id));
    $trail->push('edit', "");
});


// Admin > Music
Breadcrumbs::for('admin.music.index', function ($trail) {
    $trail->parent('admin.');
    $trail->push('music', route('admin.music.index'));
});

// Admin > DJ
Breadcrumbs::for('admin.dj.index', function ($trail) {
    $trail->parent('admin.');
    $trail->push('dj', route('admin.dj.index'));
});

// Admin > Dj > Create
Breadcrumbs::for('admin.dj.create', function ($trail) {
    $trail->parent('admin.');
    $trail->push('dj', route('admin.dj.index'));
    $trail->push('create', route('admin.dj.create'));
});

// Admin > Video
Breadcrumbs::for('admin.video.index', function ($trail) {
    $trail->parent('admin.');
    $trail->push('video', route('admin.video.index'));
});

// Admin > Video > Create
Breadcrumbs::for('admin.video.create', function ($trail) {
    $trail->parent('admin.');
    $trail->push('video', route('admin.video.index'));
    $trail->push('create', route('admin.video.create'));
});

// Admin > Video > Edit > [$article_id]
Breadcrumbs::for('admin.video.edit', function ($trail,$id) {
    $trail->parent('admin.');
    $trail->push('video', route('admin.video.index'));
    $trail->push($id, route('admin.video.edit', $id));
    $trail->push('edit', "");
});

// Admin > Portfolio
Breadcrumbs::for('admin.portfolio.index', function ($trail) {
    $trail->parent('admin.');
    $trail->push('portfolio');
});

// Admin > Portfolio > Create
Breadcrumbs::for('admin.portfolio.create', function ($trail) {
    $trail->parent('admin.');
    $trail->push('portfolio');
    $trail->push('create', route('admin.portfolio.create'));
});

// Admin > Portfolio > Edit > [$portfolio_id]
Breadcrumbs::for('admin.portfolio.edit', function ($trail,$portfolio_id) {
    $trail->parent('admin.');
    $trail->push('portfolio');
    $trail->push($portfolio_id, route('admin.portfolio.edit', $portfolio_id));
    $trail->push('edit', "");
});

// Admin > Content
Breadcrumbs::for('admin.content.index', function ($trail) {
    $trail->parent('admin.');
    $trail->push('content');
});

// Admin > Users
Breadcrumbs::for('admin.users', function ($trail) {
    $trail->parent('admin.');
    $trail->push('users');
});


