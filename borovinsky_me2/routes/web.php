<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group([
    'prefix' => LocalizationService::locale(),
    'middleware' => 'setLocale',
        ], function () {

    Route::get('/', 'IndexController@index')->name('home');
//    Route::get('/portfolio', 'IndexController@portfolio')->name('portfolio');
    //group prefix не работает написал руками portfolio
    Route::get('/portfolio/articles', 'IndexController@articles')->name('articles');
    Route::get('/portfolio/arts', 'IndexController@arts')->name('arts');
    Route::get('/portfolio/music', 'IndexController@music')->name('music');
    Route::get('/portfolio/web-projects', 'IndexController@web')->name('web-projects');
    Route::get('/portfolio/video-projects', 'IndexController@video')->name('video-projects');
    Route::get('/portfolio/dj-career', 'IndexController@dj')->name('dj-career');
    Route::get('/portfolio/contacts', 'IndexController@contacts')->name('contacts');

});

Route::post('/send_email',['uses'=>'UserController@send_email','as'=>'send_email']);
Route::post('/music/all', 'Admin\Music\MusicController@musics');
Route::get('/comments/{id}', 'UserController@comments');

//Route::get('/vkapi/load_photos', 'VKapiController@loadPhotos');
//Route::get('/vkapi/parse_avatars', 'VKapiController@parseVKavatars');
//Route::post('/vkapi/process_avatars', 'VKapiController@processVKavatars');

Auth::routes();

Route::post('add_comment', 'UserController@addComment')->name('addComment');

Route::get('clear', function () {
    \Illuminate\Support\Facades\Artisan::call('config:cache');
    echo "Clear";
});


//admin
Route::get('admin/web/create', 'Admin\Web\WebController@create');
Route::prefix('admin')->as('admin.')->group(function () {
    Route::group(['middleware' => ['auth','admin']], function () {
        //resources +
        Route::post('/art/all', 'Admin\Art\ArtController@arts');
        Route::post('/dj/all', 'Admin\Dj\DjController@djs');
        Route::post('/content/all', 'Admin\Content\TextContentController@contents');
        Route::post('/content/load_file', 'Admin\Content\TextContentController@load_file');
        Route::post('/content/save_file', 'Admin\Content\TextContentController@save_file');
        Route::get('/portfolio/rm/{id}', 'Admin\Portfolio\PortfolioController@remove');
        //суперкостыль для tinyMCE (пути к фото)
        Route::get('/web/{id}', 'Admin\Web\WebController@editCr');

        //other
        Route::get('/users', 'Admin\AdminController@users');
        Route::post('/upload_image', 'Admin\AdminController@upload_public_image');

        Route::get('/', 'Admin\AdminController@index');
        Route::post('/header', 'Admin\AdminController@saveHeader');
        Route::resources([
            'portfolio' => 'Admin\Portfolio\PortfolioController',
            'article' => 'Admin\Articles\ArticleController',
            'article_category' => 'Admin\Articles\ArticleCategoryController',
            'hashtag' => 'Admin\Articles\HashtagController',
            'art' => 'Admin\Art\ArtController',
            'art_category' => 'Admin\Art\ArtCategoryController',
            'web' => 'Admin\Web\WebController',
            'web_category' => 'Admin\Web\WebCategoryController',
            'music' => 'Admin\Music\MusicController',
            'music_genre' => 'Admin\Music\GenreController',
            'dj' => 'Admin\Dj\DjController',
            'dj_category' => 'Admin\Dj\DjCategoryController',
            'video' => 'Admin\Video\VideoController',
            'content' => 'Admin\Content\TextContentController',
        ]);
    });
});

