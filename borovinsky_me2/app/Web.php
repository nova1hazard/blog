<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Web extends Model
{
    protected $guarded = [];

    protected $casts = [
        'created_at' => 'datetime:d.m.y [h:i]',
        'updated_at' => 'datetime:d.m.y [h:i]',
    ];

    public function category()
    {
        return $this->belongsTo(WebCategory::class);
    }
}
