<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Art extends Model
{
    protected $guarded = [];

    protected $table = 'arts';

    protected $casts = [
        'created_at' => 'datetime:d.m.y h:i',
        'updated_at' => 'datetime:d.m.y h:i',
    ];

    public function category()
    {
        return $this->belongsTo(ArtCategory::class);
    }
}
