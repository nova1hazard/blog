<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comments extends Model
{
    protected $guarded = [];

    protected $casts = [
        'created_at' => 'datetime:F jS Y',
        'updated_at' => 'datetime:F jS Y',
    ];

    public function owner()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
