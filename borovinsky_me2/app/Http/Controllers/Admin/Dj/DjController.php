<?php

namespace App\Http\Controllers\Admin\Dj;

use App\Dj;
use App\DjCategory;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Services\ImageResize;

class DjController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.dj.index')->with([
            'djs' => Dj::all(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function djs()
    {
        return response(['djs' => Dj::with('category')->get(),
            'categories' => DjCategory::all()]);
    }

    public function create()
    {
        return view('admin.dj.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,ImageResize $imageResize)
    {
        $path = "";
        $data = $request->except('image','token');
        if($request->hasFile('image')){
            $img_resized = $imageResize->resize($request->file('image'),300,'dj');
            $data['image_sm'] = $img_resized;
            $path = $request->file('image')->store('dj','public');
        }
        $data['image'] = $path;

        $new = Dj::create($data);

        return response(['new' => $new, 'path' => $path]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $upd =  Dj::find($id);
        $data = $request->except('image','token','_method');

//        if($request->hasFile('image')){
//            Storage::disk('public')->delete($upd->image);
//            $data['image'] = $request->file('image')->store('dj','public');
//        }

        $upd->update($data);

        return $upd;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $destroyed = Dj::find($id);
        Storage::disk('public')->delete($destroyed->image);
        Storage::disk('public')->delete($destroyed->image_sm);
        $destroyed->delete();
    }
}
