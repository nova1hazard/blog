<?php

namespace App\Http\Controllers\Admin\Web;

use App\Web;
use App\WebCategory;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class WebController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.web.index')->with([
            'webs' => Web::with('category')->orderBy('project_date')->get(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.web.create')->with([
            'without_vue' => true,
            'web_categories' => WebCategory::all(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Web::create($request->except('token'));

        return redirect('admin/web')->with('status', 'Web-project created!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.web.edit')->with([
            'without_vue' => true,
            'web' => Web::where('id', $id)->with('category')->firstorfail(),
            'web_categories' => WebCategory::all(),
        ]);
    }

    public function editCr($id)
    {
        return view('admin.web.edit')->with([
            'without_vue' => true,
            'web' => Web::where('id', $id)->with('category')->firstorfail(),
            'web_categories' => WebCategory::all(),
        ]);
    }

    public function update(Request $request, $id)
    {
        $upd =  Web::find($id);
        $upd->update($request->except('token', 'category'));

        return redirect('admin/web')->with('status', 'Web-projects updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Web::destroy($id);

        return redirect('admin/web')->with('status', 'Web-project destroyed!');
    }
}
