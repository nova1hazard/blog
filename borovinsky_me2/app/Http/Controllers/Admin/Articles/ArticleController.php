<?php

namespace App\Http\Controllers\Admin\Articles;

use App\Article;
use App\ArticleCategory;
use App\Hashtag;
use Illuminate\Http\Request;
use App\Header;
use App\Http\Controllers\Controller;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.articles.index')->with([
            'articles' => Article::with('category','hashtag')->get(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.articles.create')->with([
            'articles_categories' => ArticleCategory::all(),
            'without_vue' => true,
            'hashtags' => Hashtag::all(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $new = Article::create($request->except('token', 'hashtags'));
        //hashtags save
        if ($request->hashtags) {
            $created = Article::find($new->id);
            foreach ($request->hashtags as $hashtag){
                $created->hashtag()->attach($hashtag);
            }
        }

        return redirect('admin/article')->with('status', 'Article created!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.articles.edit')->with([
            'without_vue' => true,
            'article' => Article::where('id', $id)->with('category','hashtag')->firstorfail(),
            'articles_categories' => ArticleCategory::all(),
            'hashtags' => Hashtag::all(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $upd =  Article::find($id);
        $upd->update($request->except('token', 'hashtags','category'));
        //hashtags save
        if ($request->hashtags) {
            //detach old
            foreach (Hashtag::all() as $hashtag){
                $upd->hashtag()->detach($hashtag);
            }
            //attach new
            foreach ($request->hashtags as $hashtag){
                $upd->hashtag()->attach($hashtag);
            }
        }

        return redirect('admin/article')->with('status', 'Article updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Article::destroy($id);
        return redirect('admin/article')->with('status', 'Article destroyed!');
    }

}
