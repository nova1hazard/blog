<?php

namespace App\Http\Controllers\Admin\Video;

use App\Http\Controllers\Controller;
use App\Video;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;

class VideoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.video.index')->with([
            'videos' => Video::all(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.video.create')->with([
            'videos' => Video::all(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->except('image','video','hashtags','token');

        if($request->hasFile('image')){
            $data['image'] = $request->file('image')->store('video_covers','public');
        }
        if($request->hasFile('video')){
            $data['video'] = $request->file('video')->store('video','public');
//            $o_name = $request->file('video')->getClientOriginalName();
//            $data['video'] = $request->file('video')->storeAs('public/video', $o_name);
        }

        $new = Video::create($data);

//        //hashtags save
//        if ($request->hashtags) {
//            $created = Article::find($new->id);
//            foreach ($request->hashtags as $hashtag){
//                $created->hashtag()->attach($hashtag);
//            }
//        }

        return redirect('admin/video')->with('status', 'Video created!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.video.edit')->with([
            'video' => Video::where('id',$id)->get()[0],
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $upd =  Video::find($id);
        $data = $request->except('image','video','hashtags','token');

        if($request->hasFile('image')){
            Storage::disk('public')->delete($upd->image);
            $data['image'] = $request->file('image')->store('video_covers','public');
        }
        if($request->hasFile('video')){
            Storage::disk('public')->delete($upd->video);
//            $o_name = $request->file('video')->getClientOriginalName();
            $data['video'] = $request->file('video')->store('video','public');
//            $data['video'] = $request->file('video')->storeAs('public/video', $o_name);
        }

        $upd->update($data);

        return redirect('admin/video')->with('status', 'Video updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $destroyed = Video::where('id',$id)->get()[0];
        Storage::disk('public')->delete($destroyed->image);
        Storage::disk('public')->delete($destroyed->video);
        Video::destroy($id);

        return redirect('admin/video')->with('status', 'Video destroyed!');
    }
}
