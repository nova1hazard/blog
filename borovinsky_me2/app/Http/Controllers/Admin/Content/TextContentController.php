<?php

namespace App\Http\Controllers\Admin\Content;

use App\Http\Controllers\Controller;
use App\TextContent;
use Illuminate\Http\Request;

class TextContentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.content.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function contents()
    {
        $files = [];
        $arr = scandir('../resources/lang/en');
        unset($arr[0],$arr[1]);
        sort($arr);
        $files['en'] =$arr;

        $arr = scandir('../resources/lang/ru');
        unset($arr[0],$arr[1]);
        sort($arr);
        $files['ru'] =$arr;

        return response(
            ['files' =>  $files,
            ]);
    }

    public function load_file(Request $request)
    {
        $file_name = $request['file_name'];
        $prefix = $request['prefix'];
        $file = file_get_contents("../resources/lang/{$prefix}/{$file_name}");
        return response(
            ['file' =>  $file,
            ]);
    }

    public function save_file(Request $request)
    {
//        dd($request->except('token'));
        $content = $request['text'];
        $file_name = $request['file_name'];
        $prefix = $request['prefix'];
        file_put_contents("../resources/lang/{$prefix}/{$file_name}",$content);
    }

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
