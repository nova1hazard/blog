<?php

namespace App\Http\Controllers\Admin\Art;

use App\ArtCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Art;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use App\Services\ImageResize;

class ArtController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.art.index')->with([
            'arts' => Art::with('category')->get(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function arts()
    {
        return response(['arts' => Art::with('category')->get(),
                        'categories' => ArtCategory::all()]);
    }


    public function create()
    {
        return view('admin.art.create')->with([
            'art_categories' => ArtCategory::all(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,ImageResize $imageResize)
    {
        $path = "";
        $data = $request->except('image','token');
        if($request->hasFile('image')){
            $img_resized = $imageResize->resize($request->file('image'),300,'arts');
            $data['image_sm'] = $img_resized;
            $path = $request->file('image')->store('arts','public');
        }
        $data['image'] = $path;
        $new = Art::create($data);

        return response(['new' => $new, 'path' => $path]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $upd =  Art::find($id);
        $data = $request->except('token','image','_method');
//        if($request->hasFile('image')){
//            Storage::disk('public')->delete($upd->image);
//            $data['image'] = $request->file('image')->store('arts','public');
//        }
        $upd->update($data);

        return $upd;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $destroyed = Art::findOrFail($id);
        Storage::disk('public')->delete($destroyed->image);
        Storage::disk('public')->delete($destroyed->image_sm);
        $destroyed->delete();

        return $destroyed;
    }
}
