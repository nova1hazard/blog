<?php

namespace App\Http\Controllers\Admin;

use App\AdminSection;
use App\Article;
use App\Art;
use App\Dj;
use App\Header;
use App\HeaderLinks;
use App\Music;
use App\Portfolio;
use App\User;
use App\Video;
use App\Web;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{
    public function index()
    {
        return view('admin.index')->with([
//            'articles' => Article::with('owner')->get(),
            'portfolios' => Portfolio::all(),
            'articles_сount' => Article::all()->count(),
            'arts_сount' => Art::all()->count(),
            'djs_сount' => Dj::all()->count(),
            'webs_сount' => Web::all()->count(),
            'musics_сount' => Music::all()->count(),
            'videos_сount' => Video::all()->count(),
            'admin_sections' => AdminSection::all(),
            'header' => Header::with('link')->get(),
            'header_links' => HeaderLinks::all(),
        ]);
    }

    public function saveHeader(Request $request)
    {
        Header::truncate();
        $menu_items = $request->except('token')['menu'];
        foreach ($menu_items as $item){
            Header::create($item);
        }
        return redirect('admin/')->with('status', 'Header created!');
    }

    public function users()
    {
        return view('admin.users')->with([
            'users' => User::with('roles')->get()->toArray(),
        ]);
    }

    public function upload_public_image(Request $request)
    {
        $url = $request['file']->store('uploads','public');

        return ["location" => "../../storage/".$url];
    }
}
