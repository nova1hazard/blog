<?php

namespace App\Http\Controllers\Admin\Portfolio;

use App\HeaderLinks;
use App\Http\Controllers\Controller;
use App\Portfolio;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;

class PortfolioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.portfolio.index')->with([
//            'articles' => Article::with('owner')->get(),
            'portfolios' => Portfolio::all(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.portfolio.create')->with([
            'header_links'=> HeaderLinks::all(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->except('image', 'token');

        if ($request->hasFile('image')) {
            $data['image'] = $request->file('image')->store('portfolio', 'public');
        }

        Portfolio::create($data);

        return redirect('admin/portfolio')->with('status', 'Portfolio created!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.portfolio.edit')->with([
            'portfolio' => Portfolio::where('id', $id)->firstorfail(),
            'header_links'=> HeaderLinks::all(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $upd = Portfolio::find($id);
        $data = $request->except('image', 'token', '_method');

        if ($request->hasFile('image')) {
            Storage::disk('public')->delete($upd->image);
            $data['image'] = $request->file('image')->store('portfolio', 'public');
        }

        $upd = Portfolio::find($id);
        $upd->update($data);

        return redirect('admin/portfolio')->with('status', 'Portfolio updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }

    public function remove($id)
    {

        $destroyed = Portfolio::find($id);
        Storage::disk('public')->delete($destroyed->image);
        Portfolio::destroy($id);

        return redirect('admin/portfolio')->with('status', 'Portfolio destroyed!');
    }
}
