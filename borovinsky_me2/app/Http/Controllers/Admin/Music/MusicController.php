<?php

namespace App\Http\Controllers\Admin\Music;

use App\Genre;
use App\Http\Controllers\Controller;
use App\Music;
use App\MusicGenre;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use App\Services\ImageResize;

class MusicController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.music.index')->with([
            'musics' => Music::with('genre')->orderBy('order')->get(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function musics()
    {
        return response(
            ['musics' => Music::with('genre')->orderBy('order')->get(),
            'genres' => Genre::all()]);
    }

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, ImageResize $imageResize)
    {
        $data = $request->except('image', 'track', 'genres', 'token');

        if ($request->hasFile('track')) {

            if (!is_numeric($data['order'])) {
                $data['order'] = 0;
            }
        //img
         //только ресайзед
            if ($request->hasFile('image'))
            {
                $filename = $request->file('image')->getClientOriginalName();
                //костыль если гиф то не сжимаем чтобы не потерять анимацию
                if(preg_match('/[.](gif)$/', $filename)) {
                    $data['image'] =  $request->file('image')->store('music_covers','public');
                    $data['image'] = '/storage/'.$data['image'];
                }
                else {
                    $img_resized = $imageResize->resize($request->file('image'), 200, 'music_covers');
                    $data['image'] = '/storage/'.$img_resized;
                }

            }
            else {
                $data['image'] = '/assets/images/no-cover.gif';
            }

         //track
            $data['track'] = $request->file('track')->store('music', 'public');
            $data['track'] = '/storage/'.$data['track'];
            if ($data['duration']) {
                $data['duration'] = Carbon::parse(intval(round($data['duration'])))->format('i:s');
            }
            $new = Music::create($data);
        //genres save
            if ($request->genres) {
                $created = Music::find($new->id);
                foreach (explode(",", $request->genres) as $genre) {
                    $created->genre()->attach($genre);
                }
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //dd($request->except('token'));
        $upd = Music::find($id);
        $data = $request->except('image', 'track', 'genres', 'token', '_method');

//        if($request->hasFile('image')){
//            Storage::disk('public')->delete($upd->image);
//            $data['image'] = $request->file('image')->store('uploads','public');
//        }
//        if($request->hasFile('track')){
//            Storage::disk('public')->delete($upd->image);
//            $o_name = $request->file('track')->getClientOriginalName();
//            $data['track'] = $request->file('track')->storeAs('public/music', $o_name);
//        }

        $upd->update($data);


        //detach old
        foreach (Genre::all() as $genre) {
            $upd->genre()->detach($genre);
        }
        //genres save
        if ($request->genres) {
            //attach new
            foreach (explode(",", $request->genres) as $genre) {
                $upd->genre()->attach($genre);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $destroyed = Music::find($id);
        if($destroyed->image !=='/assets/images/no-cover.gif'){
            unlink(public_path().$destroyed->image);
        }
        unlink(public_path().$destroyed->track);
        $destroyed->delete();

        return $destroyed;
    }
}
