<?php

namespace App\Http\Controllers;

use App\Art;
use App\ArtCategory;
use App\Article;
use App\Comments;
use App\Dj;
use App\Header;
use App\HeaderLinks;
use App\Music;
use App\Portfolio;
use App\Video;
use App\Web;
use App\DjCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class IndexController extends Controller
{
    public function index()
    {
        return view('welcome')->with([
            'without_vue' => true,
            'comments' => Comments::with('owner')->get(),
            'articles' => Article::with('category','hashtag')->get(),
            'portfolios' => Portfolio::all()->sortBy('order'),
        ]);
    }

    public function portfolio()
    {
        return view('portfolio')->with([
            'portfolios' => Portfolio::all()->sortBy('order'),
        ]);
    }

    public function articles()
    {
        return view('articles')->with([
            'comments' => Comments::with('owner')->get(),
            'articles' => Article::with('category','hashtag')->get(),
            'link_id' => HeaderLinks::where('link','articles')->pluck('id')[0],
        ]);
    }

    public function arts()
    {
        return view('arts')->with([
            'link_id' => HeaderLinks::where('link','arts')->pluck('id')[0],
            'all_arts' => ArtCategory::has('art')->with('art')->orderBy('order')->get()->toArray(),
        ]);
    }

    public function music()
    {
        return view('music')->with([
            'musics' => Music::with('genre')->orderBy('order')->get(),
            'link_id' => HeaderLinks::where('link','music')->pluck('id')[0],
        ]);
    }

    public function video()
    {
        return view('video')->with([
            'videos' => Video::all(),
            'link_id' => HeaderLinks::where('link','video-projects')->pluck('id')[0],
        ]);
    }

    public function web()
    {
        return view('web')->with([
            'webs' => Web::with('category')->orderBy('project_date', 'desc')->get(),
            'link_id' => HeaderLinks::where('link','web-projects')->pluck('id')[0],
        ]);
    }

    public function dj()
    {
        return view('dj')->with([
            'all_djs' => DjCategory::has('dj')->with('dj')->orderBy('order')->get()->toArray(),
            'link_id' => HeaderLinks::where('link','dj-career')->pluck('id')[0],
        ]);
    }

    public function contacts()
    {
        return view('contacts')->with([
//            'contacts' =>  ::all(),
        ]);
    }

}
