<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx as ReaderXlsx;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx as WriterXlsx;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

class VKapiController extends Controller
{
    public $kite_mobile_token;
    public $peer_id;
    public $group_id;
    public $vvk;

    public function __construct()
    {
        $this->kite_mobile_token = '15a16b77c4fbf4d78b81da66cfc91fca647cb018a56f4446288972f7177253b864e41f9d460c86e27f825';
        $this->peer_id = '2000000123';
        $this->group_id = '136408619';
        $this->vvk = '5.74';
    }

    public function loadPhotos()
    {

        $nowtime = time();
        $week = 604800;
        $day = 86400;
        $hour = $day/12;
        $album_id = 257477255;
        $imgcount = 200;


        $photos = file_get_contents("https://api.vk.com/method/messages.getHistoryAttachments?&media_type=photo&count=$imgcount&peer_id=$this->peer_id&v=$this->vvk&access_token=$this->kite_mobile_token");
        $photos_result = json_decode($photos, true);

        for ($i = 0; $i < $imgcount; $i++) {
            if ($photos_result['response']['items'][$i]['attachment']['photo']['date'] < (time() - $day)) {
                unset($photos_result['response']['items'][$i]);
            }
        }

        $rnd = array_rand($photos_result['response']['items'], 3);

        for ($i = 0; $i < 3; $i++) {
            if (isset($photos_result['response']['items'][$rnd[$i]]['attachment']['photo']['photo_1280'])) {
                $ph_url = $photos_result['response']['items'][$rnd[$i]]['attachment']['photo']['photo_1280'];
            } elseif (isset($photos_result['response']['items'][$rnd[$i]]['attachment']['photo']['photo_604'])) {
                $ph_url = $photos_result['response']['items'][$rnd[$i]]['attachment']['photo']['photo_604'];
            } elseif (isset($photos_result['response']['items'][$rnd[$i]]['attachment']['photo']['photo_130'])) {
                $ph_url = $photos_result['response']['items'][$rnd[$i]]['attachment']['photo']['photo_130'];
            } elseif (isset($photos_result['response']['items'][$rnd[$i]]['attachment']['photo']['photo_75'])) {
                $ph_url = $photos_result['response']['items'][$rnd[$i]]['attachment']['photo']['photo_75'];
            } else {
                continue;
            }
            $contents = file_get_contents($ph_url);
            $name = basename($ph_url);
            $p = 'vk/'.$name;
            Storage::disk('public')->put($p, $contents);

            $url = file_get_contents("https://api.vk.com/method/photos.getUploadServer?group_id=".$this->group_id."&album_id=".$album_id."&v=".$this->vvk."&access_token=".$this->kite_mobile_token);
            $url = json_decode($url)->response->upload_url;

            $full_url = url('/').$p;

            $path = Storage::disk('public')->path($p);
            $result = json_decode($this->curl($url, array('file1' => new \CURLFile($path))));

            $safe = file_get_contents("https://api.vk.com/method/photos.save?album_id=".$result->aid."&group_id=".$this->group_id."&server=".$result->server."&photos_list=".$result->photos_list."&hash=".$result->hash."&v=".$this->vvk."&access_token=".$this->kite_mobile_token);
            $safe = json_decode($safe, true);

            Storage::disk('public')->delete($p);
        }
    }

    public function curl($url, $params = false)
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        if (isset($params))
        {
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        }

        $upd = curl_exec($ch);
        curl_close($ch);

        return $upd;
    }

    public function parseVKavatars(){
        return view('parse_avatars');
    }

    public function processVKavatars(Request $request){
        $links = [];

        if($request->hasFile('file')){

            $reader = new ReaderXlsx();
            $reader->setReadDataOnly(true);
            $spreadsheet = $reader->load($request->file);
            $spreadsheet->setActiveSheetIndex(0);
            $sheet = $spreadsheet->getActiveSheet();
            $k = 0;
            $spreadsheet_write = new Spreadsheet();
            $sheet_write = $spreadsheet_write->getActiveSheet();
            foreach ($sheet->getRowIterator() as $row) {
                $cellIterator = $row->getCellIterator();
                foreach ($cellIterator as $cell) {
                    $vk_id = $cell->getCalculatedValue();
                    if(is_numeric($vk_id)){
                        $format_photo = 'photo_max_orig';
                        $result = file_get_contents("https://api.vk.com/method/users.get?&user_id=$vk_id&fields=$format_photo&v=$this->vvk&access_token=".$this->kite_mobile_token);
                        $result_array = json_decode($result,true);
                        $what_replace = array('?ava=1');
                        if(!isset($result_array['response'])) {
                            continue;
                        }
                         $res_url = str_replace($what_replace, "", $result_array['response'][0][$format_photo]);
                        $sheet_write->setCellValue('A'.$k, $vk_id);
                        $sheet_write->setCellValue('B'.$k, $res_url);
                        $links[$k]['vk_id'] = $vk_id;
                        $links[$k]['avatar'] =  $res_url;
                        $k++;
                    }
                }
            }
            $writer = new WriterXlsx($spreadsheet_write);
            $writer->setPreCalculateFormulas(false);
            $writer->save('avatars.xlsx');

            return response(['links' => $links]);

        }
        else {
            echo 'File is not valid';
        }
    }
}
