<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Comments;

class UserController extends Controller
{
    public function comments($link_id)
    {
        $comments = Comments::where('link_id',$link_id)->latest()->get();
        $rate = 0;
        $rate = Comments::where('link_id',$link_id)->whereNotNull('rate')->avg('rate');
        $rate = round($rate, 2);
        return ['comments' => $comments,
                'total_rate' => $rate];
    }

    public function addComment(Request $request)
    {
        $comment = $request->except('token');
        $comment['avatar'] = random_int(0, 50).".jpg";
//        $comment += ['user_id' => Auth::user()->id];
        Comments::create($comment);
    }

    public function deleteComment($id)
    {
        Comments::destroy($id);
    }

    public function send_email(Request $request) {

        $name = $request->send_name;
        $email = $request->send_email;
        $subject = $request->send_subject;
        $send_text = $request->send_text;

        $message = "
        <html> 
        <head> 
        <title></title> 
        <STYLE> 
        .text1 {font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px; border: 1px solid #ecf9ec;} 
        .text2 {font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 18px; } 
        .text4 {font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 22px; } 
        .text3 {font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 14px; color:#3e3e3e;} 
        .border_tabl {border: 2px solid #8cd98c;} 
        </STYLE> 
        </head> 
        <body> 
        <table align='center' width='800' bgcolor='#c6ecc6' border='0' cellspacing='40' cellpadding='20' class='border_tabl'> 
        <tr> 
        <td bgcolor='#FFFFFF' class='text1' align='center'><img src='http://getandroid.ir/uploads/posts/2014-11/1415433525_chomp-sms-logo.png' width='90' height='75' alt='new_message' border='0'><br><br> 
        <span class='text2'>".$email."</span><br><br>
        <span class='text4'><strong>".$name.":</strong></span><br><br> 
        <span class='text3'>".$send_text."</span>
        </td> 
        </tr> 
        
        </table> 
        </body> 
        </html>";

        $address = "nova1hazard@gmail.com";
        $headers="Content-type: text/html; charset=\"windows-1251\" \r\n Reply-To: ..............@mail.ru \r\n";
        mail($address, $subject, $message, $headers);

    }
}
