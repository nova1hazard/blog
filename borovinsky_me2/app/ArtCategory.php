<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArtCategory extends Model
{
    protected $guarded = [];

    public function art()
    {
        return $this->hasMany(Art::class,'category_id', 'id');
    }
}
