<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Header extends Model
{
    protected $guarded = [];

    public function link()
    {
        return $this->belongsTo(HeaderLinks::class);
    }
}
