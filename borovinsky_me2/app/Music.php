<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Music extends Model
{
    protected $guarded = [];

    protected $table = 'musics';

    protected $casts = [
        'created_at' => 'datetime:d.m.y h:i',
        'updated_at' => 'datetime:d.m.y h:i',
    ];

    public function genre()
    {
        return $this->belongsToMany(Genre::class, 'music_genres')->using(MusicGenre::class);
    }
}
