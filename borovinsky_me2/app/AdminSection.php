<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdminSection extends Model
{
    protected $table = 'admin_sections';
    protected $guarded = [];
}
