<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dj extends Model
{
    protected $casts = [
        'created_at' => 'datetime:d.m.y h:i',
        'updated_at' => 'datetime:d.m.y h:i',
    ];

    protected $guarded = [];

    public function category()
    {
        return $this->belongsTo(DjCategory::class);
    }
}
