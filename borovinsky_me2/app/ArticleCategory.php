<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArticleCategory extends Model
{
    protected $guarded = [];

    public function article()
    {
        return $this->belongsTo(Article::class,'id', 'category_id');
    }
}
