<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hashtag extends Model
{
    protected $guarded = [];

    public function article()
    {
        return $this->belongsToMany(Article::class, 'article_hashtags')->using(Hashtag::class);
    }
}
