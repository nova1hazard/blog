<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\ImageResize;

class ImageResizeServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Services\ImageResize', function ($app) {
            return new ImageResize();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
