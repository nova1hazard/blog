<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 29.04.2020
 * Time: 13:11
 */

namespace App\Services\Localization;

use Illuminate\Support\Facades\Facade;

class LocalizationService extends Facade
{
    protected static function getFacadeAccessor()
    {
        return "Localization";
    }
}