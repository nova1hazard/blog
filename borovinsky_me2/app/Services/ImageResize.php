<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 25.06.2020
 * Time: 4:23
 */

namespace App\Services;


class ImageResize
{
    public function resize($img_resized,$final_width_of_image,$dir)
    {
        $filename = $img_resized->getClientOriginalName();
        $im = '';

        if(preg_match('/[.](jpg)$/', $filename)) {
            $im = imagecreatefromjpeg($img_resized);
        } else if (preg_match('/[.](gif)$/', $filename)) {
            $im = imagecreatefromgif($img_resized);
        } else if (preg_match('/[.](png)$/', $filename)) {
            $im = imagecreatefrompng($img_resized);
        }
        $ox = imagesx($im);
        $oy = imagesy($im);

        $nx = $final_width_of_image;
        $ny = floor($oy * ($final_width_of_image / $ox));
        $nm = imagecreatetruecolor($nx, $ny);

        //добавим _sm к имени файла
        $c = explode(".", $filename);
        $frandom = random_bytes(7);
        $frandom = (bin2hex($frandom));
        $filename = $frandom."_sm.".$c[1];
        imagecopyresized($nm, $im, 0,0,0,0,$nx,$ny,$ox,$oy);
        imagejpeg($nm, "storage/{$dir}/".$filename);

        return "{$dir}/{$filename}";
    }
}