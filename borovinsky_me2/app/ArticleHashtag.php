<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\Pivot;

class ArticleHashtag extends Pivot
{
    protected $guarded = [];
}
