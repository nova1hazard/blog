<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WebCategory extends Model
{
    protected $guarded = [];

    public function web()
    {
        return $this->belongsTo(Web::class,'id', 'category_id');
    }
}
