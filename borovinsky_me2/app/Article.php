<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $guarded = [];

    protected $casts = [
        'created_at' => 'datetime:d.m.y h:i',
        'updated_at' => 'datetime:d.m.y h:i',
    ];

    public function category()
    {
        return $this->belongsTo(ArticleCategory::class);
    }

    public function hashtag()
    {
        return $this->belongsToMany(Hashtag::class, 'article_hashtags')->using(ArticleHashtag::class);
    }

}
