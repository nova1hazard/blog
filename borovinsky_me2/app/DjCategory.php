<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DjCategory extends Model
{
    protected $guarded = [];

    public function dj()
    {
        return $this->hasMany(Dj::class,'category_id', 'id');
    }
}
